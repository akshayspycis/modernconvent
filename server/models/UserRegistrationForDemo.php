<?php
    class UserRegistrationForDemo{
        private $user_registration_for_demo_id;
        private $user_name;
        private $gender;
        private $email;
        private $contact_no;
        
        public function getUser_registration_for_demo_id() {
            return $this->user_registration_for_demo_id;
        }

        public function getUser_name() {
            return $this->user_name;
        }

        public function getGender() {
            return $this->gender;
        }

        public function getEmail() {
            return $this->email;
        }

        public function getContact_no() {
            return $this->contact_no;
        }

        public function setUser_registration_for_demo_id($user_registration_for_demo_id) {
            $this->user_registration_for_demo_id = $user_registration_for_demo_id;
        }

        public function setUser_name($user_name) {
            $this->user_name = $user_name;
        }

        public function setGender($gender) {
            $this->gender = $gender;
        }

        public function setEmail($email) {
            $this->email = $email;
        }

        public function setContact_no($contact_no) {
            $this->contact_no = $contact_no;
        }


}



