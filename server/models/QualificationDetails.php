<?php
    class QualificationDetails{

        private $qualification_details;
        private $class;
        private $school_college;
        private $board_university;
        private $year_of_passing;
        private $percentage;
        private $course_registration_id;
        
        function getQualification_details() {
            return $this->qualification_details;
        }

        function getClass() {
            return $this->class;
        }

        function getSchool_college() {
            return $this->school_college;
        }

        function getBoard_university() {
            return $this->board_university;
        }

        function getYear_of_passing() {
            return $this->year_of_passing;
        }

        function getPercentage() {
            return $this->percentage;
        }

        function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        function setQualification_details($qualification_details) {
            $this->qualification_details = $qualification_details;
        }

        function setClass($class) {
            $this->class = $class;
        }

        function setSchool_college($school_college) {
            $this->school_college = $school_college;
        }

        function setBoard_university($board_university) {
            $this->board_university = $board_university;
        }

        function setYear_of_passing($year_of_passing) {
            $this->year_of_passing = $year_of_passing;
        }

        function setPercentage($percentage) {
            $this->percentage = $percentage;
        }

        function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }


        
        

    }


