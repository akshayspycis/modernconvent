<?php
    class Careers{
        private $career_id;
        private $heading;
        private $content;
        private $city;
        private $date;
        private $image;
        private $posted_on;
        
        function getCareer_id() {
            return $this->career_id;
        }

        function getHeading() {
            return $this->heading;
        }

        function getContent() {
            return $this->content;
        }

        function getCity() {
            return $this->city;
        }

        function getDate() {
            return $this->date;
        }

        function getImage() {
            return $this->image;
        }

        function getPosted_on() {
            return $this->posted_on;
        }

        function setCareer_id($career_id) {
            $this->career_id = $career_id;
        }

        function setHeading($heading) {
            $this->heading = $heading;
        }

        function setContent($content) {
            $this->content = $content;
        }

        function setCity($city) {
            $this->city = $city;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setImage($image) {
            $this->image = $image;
        }

        function setPosted_on($posted_on) {
            $this->posted_on = $posted_on;
        }


      

    }




