<?php
    class LoginDetails{
        
        private $user_id;
        private $password;
        private $security_ques;
        private $answer;
        private $user_type;
        private $status;
        
        function getUser_id() {
            return $this->user_id;
        }

        function getPassword() {
            return $this->password;
        }

        function getSecurity_ques() {
            return $this->security_ques;
        }

        function getAnswer() {
            return $this->answer;
        }

        function getUser_type() {
            return $this->user_type;
        }

        function getStatus() {
            return $this->status;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setPassword($password) {
            $this->password = $password;
        }

        function setSecurity_ques($security_ques) {
            $this->security_ques = $security_ques;
        }

        function setAnswer($answer) {
            $this->answer = $answer;
        }

        function setUser_type($user_type) {
            $this->user_type = $user_type;
        }

        function setStatus($status) {
            $this->status = $status;
        }

    }


