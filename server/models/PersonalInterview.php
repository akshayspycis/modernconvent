<?php
    class PersonalInterview{
        private $personalinterview_id;
        private $exam_id;
        private $exam_sub_cat_id;
        private $exam_phase_id;
        private $content;
        private $year;
        
        function getPersonalinterview_id() {
            return $this->personalinterview_id;
        }

        function getExam_id() {
            return $this->exam_id;
        }

        function getExam_sub_cat_id() {
            return $this->exam_sub_cat_id;
        }

        function getExam_phase_id() {
            return $this->exam_phase_id;
        }

        function getContent() {
            return $this->content;
        }

        function getYear() {
            return $this->year;
        }

        function setPersonalinterview_id($personalinterview_id) {
            $this->personalinterview_id = $personalinterview_id;
        }

        function setExam_id($exam_id) {
            $this->exam_id = $exam_id;
        }

        function setExam_sub_cat_id($exam_sub_cat_id) {
            $this->exam_sub_cat_id = $exam_sub_cat_id;
        }

        function setExam_phase_id($exam_phase_id) {
            $this->exam_phase_id = $exam_phase_id;
        }

        function setContent($content) {
            $this->content = $content;
        }

        function setYear($year) {
            $this->year = $year;
        }


        
        
        

}
?>