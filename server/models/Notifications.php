<?php
    class Notifications {
        private $id;
        private $heading;
        private $link;
        private $school_id;
        private $date;
        function getId() {
            return $this->id;
        }

        function getHeading() {
            return $this->heading;
        }

        function getLink() {
            return $this->link;
        }

        function getSchool_id() {
            return $this->school_id;
        }

        function getDate() {
            return $this->date;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setHeading($heading) {
            $this->heading = $heading;
        }

        function setLink($link) {
            $this->link = $link;
        }

        function setSchool_id($school_id) {
            $this->school_id = $school_id;
        }

        function setDate($date) {
            $this->date = $date;
        }


}