<?php
    class DemoClass{
        
        private $demo_class_id;
        private $demo_class;
        private $status;
        public function getDemo_class_id() {
            return $this->demo_class_id;
        }

        public function getDemo_class() {
            return $this->demo_class;
        }

        public function getStatus() {
            return $this->status;
        }

        public function setDemo_class_id($demo_class_id) {
            $this->demo_class_id = $demo_class_id;
        }

        public function setDemo_class($demo_class) {
            $this->demo_class = $demo_class;
        }

        public function setStatus($status) {
            $this->status = $status;
        }

}



