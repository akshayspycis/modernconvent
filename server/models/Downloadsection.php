<?php
    class Downloadsection{
        private $id;
        private $name;
        private $path;
        private $date;
        
        function getId() {
            return $this->id;
        }

        function getName() {
            return $this->name;
        }

        function getPath() {
            return $this->path;
        }

        function getDate() {
            return $this->date;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setPath($path) {
            $this->path = $path;
        }

        function setDate($date) {
            $this->date = $date;
        }


    }




