<?php
    class Feedback{

        private $feedback_id;
        private $name;
        private $message;
        private $image;
        private $city;
        private $date;
        private $time;
        
        function getFeedback_id() {
            return $this->feedback_id;
        }

        function getName() {
            return $this->name;
        }

        function getMessage() {
            return $this->message;
        }

        function getImage() {
            return $this->image;
        }

        function getCity() {
            return $this->city;
        }

        function getDate() {
            return $this->date;
        }

        function getTime() {
            return $this->time;
        }

        function setFeedback_id($feedback_id) {
            $this->feedback_id = $feedback_id;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setMessage($message) {
            $this->message = $message;
        }

        function setImage($image) {
            $this->image = $image;
        }

        function setCity($city) {
            $this->city = $city;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setTime($time) {
            $this->time = $time;
        }


    }


