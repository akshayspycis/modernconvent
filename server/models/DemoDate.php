<?php
    class DemoDate{
        private $demo_date_id;
        private $demo_class_id;
        private $date;
        private $status;
        
        public function getDemo_date_id() {
            return $this->demo_date_id;
        }

        public function getDemo_class_id() {
            return $this->demo_class_id;
        }

        public function getDate() {
            return $this->date;
        }

        public function getStatus() {
            return $this->status;
        }

        public function setDemo_date_id($demo_date_id) {
            $this->demo_date_id = $demo_date_id;
        }

        public function setDemo_class_id($demo_class_id) {
            $this->demo_class_id = $demo_class_id;
        }

        public function setDate($date) {
            $this->date = $date;
        }

        public function setStatus($status) {
            $this->status = $status;
        }

}



