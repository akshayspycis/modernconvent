<?php
    class UserDemoDate{
        private $user_demo_date_ic;
        private $demo_class_id;
        private $demo_date_id;
        private $user_registration_for_demo_id;
        private $registered_temp_code;
        private $date_fo_regs;
        
        function getUser_demo_date_ic() {
            return $this->user_demo_date_ic;
        }

        function getDemo_class_id() {
            return $this->demo_class_id;
        }

        function getDemo_date_id() {
            return $this->demo_date_id;
        }

        function getUser_registration_for_demo_id() {
            return $this->user_registration_for_demo_id;
        }

        function getRegistered_temp_code() {
            return $this->registered_temp_code;
        }

        function getDate_fo_regs() {
            return $this->date_fo_regs;
        }

        function setUser_demo_date_ic($user_demo_date_ic) {
            $this->user_demo_date_ic = $user_demo_date_ic;
        }

        function setDemo_class_id($demo_class_id) {
            $this->demo_class_id = $demo_class_id;
        }

        function setDemo_date_id($demo_date_id) {
            $this->demo_date_id = $demo_date_id;
        }

        function setUser_registration_for_demo_id($user_registration_for_demo_id) {
            $this->user_registration_for_demo_id = $user_registration_for_demo_id;
        }

        function setRegistered_temp_code($registered_temp_code) {
            $this->registered_temp_code = $registered_temp_code;
        }

        function setDate_fo_regs($date_fo_regs) {
            $this->date_fo_regs = $date_fo_regs;
        }


}



