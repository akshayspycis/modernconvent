<?php
    class BlogCategoryDetails{
        
        private $blog_category_details_id;
        private $category;
        private $link;
        
        public function getBlog_category_details_id() {
            return $this->blog_category_details_id;
        }

        public function getCategory() {
            return $this->category;
        }

        public function getLink() {
            return $this->link;
        }

        public function setBlog_category_details_id($blog_category_details_id) {
            $this->blog_category_details_id = $blog_category_details_id;
        }

        public function setCategory($category) {
            $this->category = $category;
        }

        public function setLink($link) {
            $this->link = $link;
        }

    }



