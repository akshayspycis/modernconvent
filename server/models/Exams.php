<?php
    class Exams{
        private $exam_id;
        private $exam_name;
        
        function getExam_id() {
            return $this->exam_id;
        }

        function getExam_name() {
            return $this->exam_name;
        }

        function setExam_id($exam_id) {
            $this->exam_id = $exam_id;
        }

        function setExam_name($exam_name) {
            $this->exam_name = $exam_name;
        }


}
?>