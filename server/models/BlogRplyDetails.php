<?php
    class BlogRplyDetails{
        
        private $blog_rply_details_id;
        private $blog_id;
        private $user_id;
        private $comment;
        private $receiver_id;
        private $status;
        private $date;
        function getBlog_rply_details_id() {
            return $this->blog_rply_details_id;
        }

        function getBlog_id() {
            return $this->blog_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getComment() {
            return $this->comment;
        }

        function getReceiver_id() {
            return $this->receiver_id;
        }

        function getStatus() {
            return $this->status;
        }

        function getDate() {
            return $this->date;
        }

        function setBlog_rply_details_id($blog_rply_details_id) {
            $this->blog_rply_details_id = $blog_rply_details_id;
        }

        function setBlog_id($blog_id) {
            $this->blog_id = $blog_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setComment($comment) {
            $this->comment = $comment;
        }

        function setReceiver_id($receiver_id) {
            $this->receiver_id = $receiver_id;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setDate($date) {
            $this->date = $date;
        }


        
       
    }



