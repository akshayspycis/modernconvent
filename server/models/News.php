<?php
    class News {

        private $nid;
        private $nheading;
        private $ncontent;
        private $ndate;
        private $school_id;
        private $nimage;
        private $nposted;
        
        function getNid() {
            return $this->nid;
        }

        function getNheading() {
            return $this->nheading;
        }

        function getNcontent() {
            return $this->ncontent;
        }

        function getNdate() {
            return $this->ndate;
        }

        function getSchool_id() {
            return $this->school_id;
        }

        function getNimage() {
            return $this->nimage;
        }

        function getNposted() {
            return $this->nposted;
        }

        function setNid($nid) {
            $this->nid = $nid;
        }

        function setNheading($nheading) {
            $this->nheading = $nheading;
        }

        function setNcontent($ncontent) {
            $this->ncontent = $ncontent;
        }

        function setNdate($ndate) {
            $this->ndate = $ndate;
        }

        function setSchool_id($school_id) {
            $this->school_id = $school_id;
        }

        function setNimage($nimage) {
            $this->nimage = $nimage;
        }

        function setNposted($nposted) {
            $this->nposted = $nposted;
        }


      

    }


