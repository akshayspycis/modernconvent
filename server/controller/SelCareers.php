    <?php
    include_once '../models/Careers.php';
    include_once '../managers/CareersMgr.php';
    $obj = new CareersMgr();
        
    $careers = $obj->selCareers();
    $str = array();    
    while($row = $careers->fetch()){
    $arr = array(
    'career_id' => $row['career_id'], 
    'heading' => $row['heading'],             
    'content' => $row['content'],             
    'city' => $row['city'],             
    'date' => $row['date'],             
    'image' => $row['image'],             
    'posted_on' => $row['posted_on'],             
    );
    array_push($str, $arr); 
    }
        
    echo json_encode($str);
    ?>