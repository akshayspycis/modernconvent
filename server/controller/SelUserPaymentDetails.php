<?php
     include_once '../models/UserPaymentDetails.php';
     include_once '../managers/UserPaymentDetailsMgr.php';
    $obj = new UserPaymentDetailsMgr();
    $quick_query = $obj->selUserPaymentDetails();
    $str = array();    
    while($row = $quick_query->fetch()){
            $arr = array(
            'user_payment_details_id' => $row['user_payment_details_id'], 
            'registered_temp_code' => $row['registered_temp_code'], 
            'date' => $row['date'], 
            'course_registration_id' => $row['course_registration_id'], 
            'payment' => $row['payment'], 
            'email' => $row['email'],             
            'paid' => $row['paid'],             
            'balance' => $row['balance'],             
            'user_name' => $row['user_name'],             
            'gender' => $row['gender'],    
            'email' => $row['email'],    
            'contact_no' => $row['contact_no']    
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>