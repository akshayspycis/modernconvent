<?php
    include_once '../models/ImageCategory.php';
    include_once '../managers/ImageCategoryMgr.php';
    
    $image_category = new ImageCategory();    
    $image_category->setCname($_POST["cname"]);
    $image_categoryMgr = new ImageCategoryMgr();    
    if ($image_categoryMgr->insCategory($image_category)){
        echo 'Your Category is submitted successfully';
    } else {
        echo 'Error';
    }
?>
