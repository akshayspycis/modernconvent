<?php
    include_once '../models/Notifications.php'; 
    include_once '../managers/NotificationsMgr.php'; 
    
    
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $notifications = new Notifications();
    $notifications->setHeading($_POST["heading"]);
    $notifications->setLink($_POST["link"]);  
    $notificationsMgr = new NotificationsMgr();
    if ($notificationsMgr->insNotifications($notifications)) {
        echo 'Notifications inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>
