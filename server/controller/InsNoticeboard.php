<?php
    include_once '../models/Noticeboard.php'; 
    include_once '../managers/NoticeboardMgr.php'; 
    
    
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $noticeboard = new Noticeboard();
    $noticeboard->setHeading($_POST["heading"]);
    $noticeboard->setContent($_POST["content"]);  
    $noticeboardMgr = new NoticeboardMgr();
    if ($noticeboardMgr->insNoticeboard($noticeboard)) {
        echo 'Notice inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>
