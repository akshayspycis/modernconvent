<?php
    include_once '../models/CourseBatchDetails.php'; 
    include_once '../managers/CourseBatchDetailsMgr.php'; 
    $course_batch_details = new CourseBatchDetails();
    $course_batch_details->setCourse_id($_POST["course_id"]);
    $course_batch_details->setDate($_POST["date"]);  
    $course_batch_details->setDay_type($_POST["day_type"]);   
    $course_batch_details->setDay($_POST["day"]);   
    $course_batch_details->setTime(date("g:i a", strtotime($_POST["time"])));   
    $course_batch_details->setFees($_POST["fees"]);   
    $course_batch_details->setStatus($_POST["status"]);   
    $course_batch_details->setDiscout($_POST["discount"]);   
    $course_batch_detailsMgr = new CourseBatchDetailsMgr();
    if ($course_batch_detailsMgr->insCourseBatchDetails($course_batch_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>