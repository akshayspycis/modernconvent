<?php
     include_once '../models/QuickQuery.php';
     include_once '../managers/QuickQueryMgr.php';
    $obj = new QuickQueryMgr();
    $quick_query = $obj->selQuickQuery($_POST['school_id']);
    $str = array();    
    while($row = $quick_query->fetch()){
            $arr = array(
            'quick_query_id' => $row['quick_query_id'], 
            'name' => $row['name'], 
            'email' => $row['email'],             
            'contact' => $row['contact'],             
            'subject' => $row['subject'],             
            'message' => $row['message'],             
            'school_name' => $row['school_name'],             
            'date' => $row['date']    
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>