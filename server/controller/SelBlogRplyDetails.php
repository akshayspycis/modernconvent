<?php
     include_once '../models/BlogRplyDetails.php';
    include_once '../managers/BlogRplyDetailsMgr.php';
    $obj = new BlogRplyDetailsMgr();
    $course_batch_details = $obj->selBlogRplyDetails($_POST["blog_details_id"]);
    $str = array();    
    while($row = $course_batch_details->fetch()){
            $arr = array(
            'blog_rply_details_id' => $row['blog_rply_details_id'], 
            'blog_id' => $row['blog_id'], 
            'user_id' => $row['user_id'], 
            'user_name' => $row['user_name'], 
            'comment' => $row['comment'], 
            'receiver_id' => $row['receiver_id'],
            'status' => $row['status'],
            'date' => $row['date'],
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>