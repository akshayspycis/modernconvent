<?php
    include_once '../managers/ImageCategoryMgr.php';
    
    $obj = new ImageCategoryMgr();
    $category = $obj->selImageCategory();
    
    $str = array();    
    while($row = $category->fetch()) {
        $arr = array(
            'c_id' => $row['c_id'], 
            'name' => $row['cname'],             
            
        );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>

