<?php
    include_once '../models/CurrentOpenings.php'; 
    include_once '../managers/CurrentOpeningsMgr.php'; 
    $current_openings = new CurrentOpenings();
    $current_openings->setTitle($_POST["title"]);  
    $current_openings->setDate(date("d-m-Y", strtotime($_POST["date"])));   
    $current_openings->setDiscription($_POST["discription"]);   
    $current_openings->setStatus($_POST["status"]);   
    $current_openingsMgr = new CurrentOpeningsMgr();
    if ($current_openingsMgr->insCurrentOpenings($current_openings)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>