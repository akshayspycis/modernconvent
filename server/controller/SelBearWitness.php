<?php
     include_once '../models/BearWitness.php';
    include_once '../managers/BearWitnessMgr.php';
    $obj = new BearWitnessMgr();
    $course_batch_details = $obj->selBearWitness($_POST['course_registration_id']);
    $str = array();    
    while($row = $course_batch_details->fetch()){
        $arr = array(
            'attestation' => $row['attestation'], 
            'pic_path' => $row['pic_path']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>