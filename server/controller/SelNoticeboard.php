<?php
    include_once '../models/Noticeboard.php';
    include_once '../managers/NoticeboardMgr.php';
        $obj = new NoticeboardMgr();
        $noticeboard = $obj->selNoticeboard();
        $str = array();    
        while($row = $noticeboard->fetch()){
            $arr = array(
                'id' => $row['id'], 
                'heading' => $row['heading'],             
                'content' => $row['content'],             
                 $date = date_create($row['date']),
                'date' => date_format($date, 'd/m/Y g:i A'),           
                        );
            array_push($str, $arr); 
        }
        
    echo json_encode($str);
?>



