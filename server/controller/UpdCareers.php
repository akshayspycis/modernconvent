 <?php
    include_once '../models/Careers.php';
    include_once '../managers/CareersMgr.php';
    $careers = new Careers();    
    $careers->setCareer_id($_POST["career_id"]);
    $careers->setHeading($_POST["heading"]);
    $careers->setContent($_POST["content"]);
    $careers->setCity($_POST["city"]);
    $careers->setDate($_POST["date"]);
    
    $careersMgr = new CareersMgr();    
    if ($careersMgr->updateCareers($careers)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>