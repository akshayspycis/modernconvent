<?php
    include_once '../models/Careers.php'; 
    include_once '../managers/CareersMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $careers = new Careers();
    $careers->setHeading($_POST["heading"]);
    $careers->setContent($_POST["content"]); 
    $careers->setCity($_POST["city"]); 
    $careers->setDate($date->format('d M Y'));
    $upload_dir = "upload/";
    $img = $_POST["image"];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir . time() . ".png";
    $success = file_put_contents($file, $data);
    $careers->setImage($file);   
    $careersMgr = new CareersMgr();
    if ($careersMgr->insCareers($careers)) {
        echo 'Careers inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>

