<?php
     include_once '../models/WorkExperience.php';
    include_once '../managers/WorkExperienceMgr.php';
    $obj = new WorkExperienceMgr();
    $course_batch_details = $obj->selWorkExperience($_POST['course_registration_id']);
    $str = array();    
    while($row = $course_batch_details->fetch()){
        $arr = array(
            'organization' => $row['organization'], 
            'time' => $row['time'], 
            'designation' => $row['designation']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>