 <?php
    include_once '../models/Feedback.php';
    include_once '../managers/FeedbackMgr.php';
    $feedback = new Feedback();    
    $feedback->setFeedback_id($_POST["feedback_id"]);
    $feedback->setName($_POST["name"]);
    $feedback->setMessage($_POST["message"]);
    $feedback->setCity($_POST["city"]);
    
    $feedbackMgr = new FeedbackMgr();    
    if ($feedbackMgr->updateFeedback($feedback)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>