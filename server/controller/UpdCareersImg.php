<?php
    include_once '../models/Careers.php';
    include_once '../managers/CareersMgr.php';
    $careers = new Careers(); 
    $careers->setCareer_id($_POST['career_id']);
    $upload_dir = "upload/";
    $img = $_POST['image'];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir . time() . ".png";
    $success = file_put_contents($file, $data);
    $careers->setImage($file);
    $careersMgr = new CareersMgr(); 
    if ($careersMgr->updCareersImg($careers)) {
     echo 'true';
    } else {
        echo 'Manager Error Found';        
    }
?>
