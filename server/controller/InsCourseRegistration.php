<?php
    include_once '../models/CourseRegistration.php'; 
    include_once '../models/CourseOptedDetails.php'; 
    include_once '../models/QualificationDetails.php'; 
    include_once '../models/LanguageProficiency.php'; 
    include_once '../models/WorkExperience.php'; 
    include_once '../models/AddressDetails.php'; 
    include_once '../models/BearWitness.php'; 
    include_once '../models/CourseOptedDetails.php'; 
    include_once '../managers/CourseRegistrationMgr.php'; 
    include_once '../managers/AddressDetailsMgr.php';  
    include_once '../managers/QualificationDetailsMgr.php';  
    include_once '../managers/WorkExperienceMgr.php';  
    include_once '../managers/LanguageProficiencyMgr.php';  
    include_once '../managers/CourseOptedDetailsMgr.php';  
    include_once '../managers/BearWitnessMgr.php';  
    include_once '../managers/UserPaymentDetailsMgr.php';  
    
    $course_registration_josn= json_decode($_POST['course_registration']);
    $course_registration = new CourseRegistration();
    $course_registration->setDob($course_registration_josn->dob);
    $course_registration->setUser_registration_for_demo_id($course_registration_josn->user_registration_for_demo_id);  
    $course_registration->setAdditional_qualification($course_registration_josn->additional_qualification);   
    $course_registration->setComputer_literacy($course_registration_josn->computer_literacy);   
    $course_registration->setAddress_of_institiute_or_company($course_registration_josn->address_of_institiute_or_company);   
    $course_registration->setRelative_name($course_registration_josn->relative_name);   
    $course_registration->setRelative_occupation($course_registration_josn->relative_occupation);   
    $course_registration->setApplicant_occupation($course_registration_josn->applicant_occupation);   
    $course_registration->setSchool($course_registration_josn->school);   
    $course_registration->setCollege($course_registration_josn->college);   
    $course_registration->setRelative_contact_no($course_registration_josn->relative_contact_no);   
    $course_registration->setReferal_name($course_registration_josn->referal_name);   
    $course_registration->setReferal_contact_no($course_registration_josn->referal_contact_no);   
    
    $course_registrationMgr = new CourseRegistrationMgr();
    if ($course_registrationMgr->insCourseRegistration($course_registration)) {
        $address_details=new AddressDetails();
        
        $address_details->setAddress_type("Permanent");
        $address_details->setAddress($course_registration_josn->address_details->Permanent->address);
        $address_details->setStreet($course_registration_josn->address_details->Permanent->street);
        $address_details->setCity($course_registration_josn->address_details->Permanent->city);
        $address_details->setPincode($course_registration_josn->address_details->Permanent->pincode);
        $address_details->setState($course_registration_josn->address_details->Permanent->state);
        $addressdetailsmgr=new AddressDetailsMgr();
        $addressdetailsmgr->insAddressDetails($address_details);
        $address_details->setAddress_type("Local");
        $address_details->setAddress($course_registration_josn->address_details->Local->address);
        $address_details->setStreet($course_registration_josn->address_details->Local->street);
        $address_details->setCity($course_registration_josn->address_details->Local->city);
        $address_details->setPincode($course_registration_josn->address_details->Local->pincode);
        $address_details->setState($course_registration_josn->address_details->Local->state);
        if ($addressdetailsmgr->insAddressDetails($address_details)) {
            foreach($course_registration_josn->qualification_details as $val){
                $qualification_details=new QualificationDetails();
                $qualification_details->setClass($val->class);
                $qualification_details->setSchool_college($val->school_college);
                $qualification_details->setBoard_university($val->board_university);
                $qualification_details->setYear_of_passing($val->year_of_passing);
                $qualification_details->setPercentage($val->percentage);
                $qualification_details_mgr=new QualificationDetailsMgr();
                $qualification_details_mgr->insQualificationDetails($qualification_details);
            }
            foreach($course_registration_josn->work_experience as $val){
                $work_experience=new WorkExperience();
                $work_experience->setOrganization($val->organization);
                $work_experience->setTime($val->time);
                $work_experience->setDesignation($val->designation);
                $work_experience_mgr=new WorkExperienceMgr();
                $work_experience_mgr->insWorkExperience($work_experience);
            }
            foreach($course_registration_josn->language_proficiency as $val){
                $language_proficiency=new LanguageProficiency();
                $language_proficiency->setLanguage($val->language);
                $language_proficiency->setRead($val->read);
                $language_proficiency->setWrite($val->write);
                $language_proficiency->setSpeak($val->speak);
                $language_proficiency_mgr=new LanguageProficiencyMgr();
                $language_proficiency_mgr->insLanguageProficiency($language_proficiency);
            }
                $course_opted_details=new CourseOptedDetails();
                $course_opted_details->setCourse_po_mt_and_clerk($course_registration_josn->course_opted_details->course_po_mt_and_clerk);
                $course_opted_details->setAdvance_comm_english($course_registration_josn->course_opted_details->advance_comm_english);
                $course_opted_details->setSsc_and_railways($course_registration_josn->course_opted_details->ssc_and_railways);
                $course_opted_details->setEnglish_for_competitive_exams($course_registration_josn->course_opted_details->english_for_competitive_exams);
                $course_opted_details->setPersonal_interviews($course_registration_josn->course_opted_details->personal_interviews);
                $course_opted_details->setMba_entrance_exam($course_registration_josn->course_opted_details->mba_entrance_exam);
                $course_opted_details_mgr=new CourseOptedDetailsMgr();
                $course_opted_details_mgr->insCourseOptedDetails($course_opted_details);
                foreach($course_registration_josn->bear_witness as $val){
                    $bear_witness=new BearWitness();
                    $bear_witness->setAttestation($val->attestation);
                    $bear_witness->setPic_path($val->pic_path);
                    $bear_witness_mgr=new BearWitnessMgr();
                    $bear_witness_mgr->insBearWitness($bear_witness);
                }
                $user_payment_details_mgr=new UserPaymentDetailsMgr();
                echo $user_payment_details_mgr->insUserPaymentDetails();
        }
    } else {
        echo 'Error';
    }
?>