<?php
    include_once '../models/Notifications.php';
    include_once '../managers/NotificationsMgr.php';
        $obj = new NotificationsMgr();
        
        $notifications = $obj->selNotifications();
        $str = array();    
        while($row = $notifications->fetch()){
            $arr = array(
                'id' => $row['id'], 
                'heading' => $row['heading'],             
                'link' => $row['link'],             
                'school_id' => $row['school_id'],             
                'school_name' => $row['school_name'],           
                 $date = date_create($row['date']),
                'date' => date_format($date, 'd/m/Y g:i A'),           
                        );
            array_push($str, $arr); 
        }
        
    echo json_encode($str);
?>

