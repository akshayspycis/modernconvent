<?php
    include_once '../models/ExamsPhases.php'; 
    include_once '../managers/ExamsPhasesMgr.php'; 
    $examsphases = new ExamsPhases();
    $examsphases->setExam_phase_name($_POST["exam_phase_name"]);
    $examsphases->setExam_id($_POST["selExamName"]);
    $examsphases->setExam_sub_cat_id($_POST["selExamSubCat"]);
    $examsphasesMgr = new ExamsPhasesMgr();
    if ($examsphasesMgr->insExamsPhases($examsphases)) {
        echo 'Exams inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>

