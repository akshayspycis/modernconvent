<?php
    include_once '../models/ExamsPhases.php';
    include_once '../managers/ExamsPhasesMgr.php';
        $obj = new ExamsPhasesMgr();
        $exams = $obj->selExamsPhases();
        $str = array();    
        while($row = $exams->fetch()){
            $arr = array(
                'exam_phase_id' => $row['exam_phase_id'], 
                'exam_name' => $row['exam_name'],
                'exam_sub_cat_name' => $row['exam_sub_cat_name'],
                'exam_phase_name' => $row['exam_phase_name'],
                );
            array_push($str, $arr); 
        }
        
    echo json_encode($str);
?>




