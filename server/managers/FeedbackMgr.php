<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class FeedbackMgr{    

        //method to insert feedback in database
        public function insFeedback(Feedback $feedback) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO feedback(name, message, image, city, date, time) VALUES ('".$feedback->getName()."','".$feedback->getMessage()."','".$feedback->getImage()."','".$feedback->getCity()."','".$feedback->getDate()."','".$feedback->getTime()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delFeedback($feedback_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from feedback where feedback_id = '".$feedback_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Feedback from database
        public function selFeedback() {
            $dbh = new DatabaseHelper();
            $sql = "select * from feedback";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateFeedback(Feedback $feedback) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE feedback SET " 
                    ."feedback_id='".$feedback->getFeedback_id()."',"."name='".$feedback->getName()."',"
                    ."message='".$feedback->getMessage()."',"
                    ."city='".$feedback->getCity()."'"
                    ."WHERE feedback_id=".$feedback->getFeedback_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          public function updFeedbackImg(Feedback $feedback) {
            $dbh = new DatabaseHelper();
            $sql = "SELECT feedback.image FROM feedback WHERE feedback_id=".$feedback->getFeedback_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $file;
            while($row = $stmt->fetch()) {
                $file=$row['image'];
            }
            if(file_exists($file)&&unlink($file)){
                $sql ="UPDATE feedback SET " 
                ."image='".$feedback->getImage()."'"
                ."WHERE feedback_id=".$feedback->getFeedback_id()."";
                $stmt = $dbh->createConnection()->prepare($sql);
                $i = $stmt->execute();
                $dbh->closeConnection();
                    if ($i > 0) {                
                        return TRUE;
                    } else {
                        return FALSE;
                    }
            }else{
                return FALSE;
            }
        } 
    }
?>

