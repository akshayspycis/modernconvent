<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class ExamsSectionsMgr {    
        //method to insert examssections in database
        public function insExamsSections(ExamsSections $examssections) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO examssections(exam_section_name,exam_phase_id,exam_sub_cat_id,exam_id) VALUES ('".$examssections->getExam_section_name()."','".$examssections->getExam_phase_id()."','".$examssections->getExam_sub_cat_id()."','".$examssections->getExam_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

      
        
        //method to select ExamsSections from database
        public function selExamsSections($exam_phase_id) {
            
            $dbh = new DatabaseHelper();
            $sql="";
           if($exam_phase_id==""){
               $sql = "SELECT *,(select exam_name from exams where exam_id=es.exam_id) as exam_name "
                    . ",(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=es.exam_sub_cat_id) as exam_sub_cat_name"
                    . ",(select exam_phase_name from examsphases where exam_phase_id=es.exam_phase_id) as exam_phase_name"
                    . " FROM examssections as es";
           }
           else{
                $sql = "select * from examssections where exam_phase_id ='".$exam_phase_id."'";
           }
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select ExamsSections from database
        public function selExamsSections2($exam_id) {
            $dbh = new DatabaseHelper();
           
            $sql = "select * from examssections where exam_id ='".$exam_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update ExamsSections in database
        public function updateExamsSections(ExamsSections $examssections){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE examssections SET " 
                    ."exam_sub_cat_name='".$examssections->getExam_sub_cat_name()."',"
                    ."exam_id='".$examssections->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$examssections->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete examssections in database
        public function delExamsSections($exam_section_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from  examssections where exam_section_id = '".$exam_section_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


