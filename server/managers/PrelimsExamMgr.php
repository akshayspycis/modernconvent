<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class PrelimsExamMgr{    
        //method to insert prelimsexam in database
       public function insPrelimsExam(PrelimsExam $prelimsexam){
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO prelimsexam("
                    . "exam_id, "
                    . "exam_sub_cat_id, "
                    . "exam_phase_id, "
                    . "exam_section_id, "
                    . "no_of_questions, "
                    . "max_marks, "
                    . "duration, "
                    . "gen, "
                    . "obc, "
                    . "sc, "
                    . "st, "
                    . "ph, "
                    . "vh, "
                    . "year)"
                    . "VALUES ('".$prelimsexam->getExam_id()."',"
                    . "'".$prelimsexam->getExam_sub_cat_id()."',"
                    . "'".$prelimsexam->getExam_phase_id()."',"
                    . "'".$prelimsexam->getExam_section_id()."',"
                    . "'".$prelimsexam->getNo_of_questions()."',"
                    . "'".$prelimsexam->getMax_marks()."',"
                    . "'".$prelimsexam->getDuration()."',"
                    . "'".$prelimsexam->getGen()."',"
                    . "'".$prelimsexam->getObc()."',"
                    . "'".$prelimsexam->getSc()."',"
                    . "'".$prelimsexam->getSt()."',"
                    . "'".$prelimsexam->getPh()."',"
                    . "'".$prelimsexam->getVh()."',"
                    . "'".$prelimsexam->getYear()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
       //method to select PrelimsExam from database
        public function selPrelimsExam($prelimsexam_id) {
            
            $dbh = new DatabaseHelper();
            $sql="";
           if($prelimsexam_id==""){
               $sql = "SELECT *,(select exam_name from exams where exam_id=pe.exam_id) as exam_name "
                    . ",(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=pe.exam_sub_cat_id) as exam_sub_cat_name"
                    . ",(select exam_phase_name from examsphases where exam_phase_id=pe.exam_phase_id) as exam_phase_name"
                    . ",(select exam_section_name from examssections where exam_section_id=pe.exam_section_id) as exam_section_name"
                    . " FROM prelimsexam as pe";
           }
           else{
                $sql = "select * from prelimsexam where prelimsexam_id ='".$prelimsexam_id."'";
           }
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
      
        //        method to update PrelimsExam in database
        public function updatePrelimsExam(PrelimsExam $prelimsexam){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE prelimsexam SET " 
                    ."exam_sub_cat_name='".$prelimsexam->getExam_sub_cat_name()."',"
                    ."exam_id='".$prelimsexam->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$prelimsexam->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete prelimsexam in database
        public function delPrelimsExam($prelimsexam_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from  prelimsexam where prelimsexam_id = '".$prelimsexam_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


