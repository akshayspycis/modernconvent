<?php
    include_once '../dbhelper/DatabaseHelper.php';
        class BearWitnessMgr{    
        //method to insert bear_witness in database
        public function insBearWitness(BearWitness $bear_witness) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO bear_witness( "
                    . "attestation,"
                    . "pic_path,"
                    . "course_registration_id) "
                    . "VALUES ('".$bear_witness->getAttestation()."',"
                    . "'".$bear_witness->getPic_path()."',"
                    . "(SELECT MAX(course_registration_id) FROM course_registration))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delBearWitness($bear_witness_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from bear_witness where bear_witness_id = '".$bear_witness_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select BearWitness from database
        public function selBearWitness($course_registration_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from bear_witness where course_registration_id=".$course_registration_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateBearWitness(BearWitness $bear_witness) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE bear_witness SET " 
                    ."course_id='".$bear_witness->getCourse_id()."',"
                    ."date='".$bear_witness->getDate()."',"
                    ."day_type='".$bear_witness->getDay_type()."',"
                    ."day='".$bear_witness->getDay()."',"
                    ."time='".$bear_witness->getTime()."',"
                    ."fees='".$bear_witness->getFees()."',"
                    ."status='".$bear_witness->getStatus()."',"
                    ."discount='".$bear_witness->getDiscout()."'"
                    ."WHERE bear_witness_id=".$bear_witness->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updBearWitnesstatus(BearWitness $bear_witness) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE bear_witness SET " 
                    ."status='".$bear_witness->getStatus()."'"
                   ."WHERE bear_witness_id=".$bear_witness->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
