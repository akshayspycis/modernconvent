<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class ExamsSubCategoryMgr {    
        //method to insert examssubcategory in database
        public function insExamsSubCategory(ExamsSubCategory $examssubcategory) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO examssubcategory(exam_sub_cat_name,exam_id) VALUES ('".$examssubcategory->getExam_sub_cat_name()."','".$examssubcategory->getExam_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

      
        
        //method to select ExamsSubCategory from database
        public function selExamsSubCategory() {
            $dbh = new DatabaseHelper();
           
            $sql = "SELECT examssubcategory.exam_sub_cat_id,examssubcategory.exam_sub_cat_name,exams.exam_name FROM examssubcategory INNER JOIN exams ON examssubcategory.exam_id=exams.exam_id";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select ExamsSubCategory from database
        
        //method to select ExamsSubCategory from database
        public function selExamsSubCategory2($exam_id) {
            $dbh = new DatabaseHelper();
           
            $sql = "select * from examssubcategory where exam_id ='".$exam_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update ExamsSubCategory in database
        public function updateExamsSubCategory(ExamsSubCategory $examssubcategory) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE examssubcategory SET " 
                    ."exam_sub_cat_name='".$examssubcategory->getExam_sub_cat_name()."',"
                    ."exam_id='".$examssubcategory->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$examssubcategory->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete examssubcategory in database
        public function delExamsSubCategory($exam_sub_cat_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from examssubcategory where exam_sub_cat_id = '".$exam_sub_cat_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


