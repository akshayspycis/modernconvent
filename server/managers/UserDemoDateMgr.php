
<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class UserDemoDateMgr{    
        public function insUserDemoDate(UserDemoDate $user_demo_date) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO user_demo_date( "
                    . "demo_class_id, "
                    . "demo_date_id, "
                    . "user_registration_for_demo_id, "
                    . "date_fo_regs) "
                    . "VALUES ('".$user_demo_date->getDemo_class_id()."',"
                    . "'".$user_demo_date->getDemo_date_id()."',"
                    . "(select max(user_registration_for_demo_id) from user_registration_for_demo),NOW())";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function updUserDemoDate(UserDemoDate $user_demo_date) {
            $dbh = new DatabaseHelper();
            $sql = "UPDATE user_demo_date SET registered_temp_code ='".$user_demo_date->getRegistered_temp_code()."' where user_registration_for_demo_id='".$user_demo_date->getUser_registration_for_demo_id()."'";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function delUserDemoDate($user_demo_date_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from user_demo_date where user_demo_date_id = '".$user_demo_date_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selUserDemoDate( $demo_class_id ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from user_demo_date b where status='Enable' where demo_class_id=".$demo_class_id ;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
  
    }
?>
