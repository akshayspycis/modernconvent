<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class GroupDiscussionMgr {    
        //method to insert groupdiscussion in database
        public function insGroupDiscussion(GroupDiscussion $groupdiscussion){
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO groupdiscussion("
                    . "exam_id, "
                    . "exam_sub_cat_id, "
                    . "exam_phase_id, "
                    . "content,"
                    . "year)"
                    . "VALUES ('".$groupdiscussion->getExam_id()."',"
                    . "'".$groupdiscussion->getExam_sub_cat_id()."',"
                    . "'".$groupdiscussion->getExam_phase_id()."',"
                    . "'".$groupdiscussion->getContent()."',"
                    . "'".$groupdiscussion->getYear()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

      
        
        //method to select GroupDiscussion from database
        public function selGroupDiscussion($groupdiscussion_id) {
            
            $dbh = new DatabaseHelper();
            $sql="";
           if($groupdiscussion_id==""){
               $sql = "SELECT *,(select exam_name from exams where exam_id=gd.exam_id) as exam_name "
                    . ",(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=gd.exam_sub_cat_id) as exam_sub_cat_name"
                    . ",(select exam_phase_name from examsphases where exam_phase_id=gd.exam_phase_id) as exam_phase_name"
                    . " FROM groupdiscussion as gd";
           }
           else{
                $sql = "select * from groupdiscussion where groupdiscussion_id ='".$groupdiscussion_id."'";
           }
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select GroupDiscussion from database
        public function selGroupDiscussion2($exam_id) {
            $dbh = new DatabaseHelper();
           
            $sql = "select * from groupdiscussion where exam_id ='".$exam_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update GroupDiscussion in database
        public function updateGroupDiscussion(GroupDiscussion $groupdiscussion){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE groupdiscussion SET " 
                    ."exam_sub_cat_name='".$groupdiscussion->getExam_sub_cat_name()."',"
                    ."exam_id='".$groupdiscussion->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$groupdiscussion->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete groupdiscussion in database
        public function delGroupDiscussion($groupdiscussion_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from  groupdiscussion where groupdiscussion_id = '".$groupdiscussion_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>




