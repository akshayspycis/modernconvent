<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class NoticeboardMgr {    
        //method to insert noticeboard in database
        public function insNoticeboard(Noticeboard $noticeboard) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO noticeboard(heading, content) VALUES ('".$noticeboard->getHeading()."','".$noticeboard->getContent()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete noticeboard in database
        public function delNoticeboard($id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from noticeboard where id = '".$id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Noticeboard from database
        public function selNoticeboard() {
            $dbh = new DatabaseHelper();
            $sql = "select * from noticeboard ORDER BY date DESC";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update Noticeboard in database
        public function updateNoticeboard(Noticeboard $noticeboard) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE noticeboard SET " 
                    ."heading='".$noticeboard->getHeading()."',"
                    ."content='".$noticeboard->getContent()."'"
                    ."WHERE id=".$noticeboard->getId()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
         
    }
?>

