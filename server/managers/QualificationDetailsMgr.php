<?php
    include_once '../dbhelper/DatabaseHelper.php';
    class QualificationDetailsMgr{    
        //method to insert qualification_details in database
        public function insQualificationDetails(QualificationDetails $qualification_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO qualification_details( "
                    . "class,"
                    . "school_college,"
                    . "board_university,"
                    . "year_of_passing,"
                    . "percentage,"
                    . "course_registration_id) "
                    . "VALUES ('".$qualification_details->getClass()."',"
                    . "'".$qualification_details->getSchool_college()."',"
                    . "'".$qualification_details->getBoard_university()."',"
                    . "'".$qualification_details->getYear_of_passing()."',"
                    . "'".$qualification_details->getPercentage()."',"
                    . "(SELECT MAX(course_registration_id) FROM course_registration))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delQualificationDetails($qualification_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from qualification_details where qualification_details_id = '".$qualification_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select QualificationDetails from database
        public function selQualificationDetails($course_registration_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from qualification_details where course_registration_id=".$course_registration_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateQualificationDetails(QualificationDetails $qualification_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE qualification_details SET " 
                    ."course_id='".$qualification_details->getCourse_id()."',"
                    ."date='".$qualification_details->getDate()."',"
                    ."day_type='".$qualification_details->getDay_type()."',"
                    ."day='".$qualification_details->getDay()."',"
                    ."time='".$qualification_details->getTime()."',"
                    ."fees='".$qualification_details->getFees()."',"
                    ."status='".$qualification_details->getStatus()."',"
                    ."discount='".$qualification_details->getDiscout()."'"
                    ."WHERE qualification_details_id=".$qualification_details->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updQualificationDetailstatus(QualificationDetails $qualification_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE qualification_details SET " 
                    ."status='".$qualification_details->getStatus()."'"
                   ."WHERE qualification_details_id=".$qualification_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
