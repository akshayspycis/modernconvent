<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
             <title>Modern Convent School</title>
            <meta charset="utf-8">
           
            
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <?php include 'includes/csslinks.php';?>
        </head>
       <!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('home')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">

				<!-- slideshow start -->
				<!-- ================ -->
                                <div class="banner clearfix">
                                    
                                    <!-- slideshow start -->
                                    <!-- ================ -->
                                    <div class="slideshow">
                                    
                                        <!-- slider revolution start -->
                                        <!-- ================ -->
                                        <div class="slider-banner-container" style="">
                                            <div class="slider-banner-fullwidth-big-height">
                                                <ul class="slides">
                                                    <!-- slide 1 start -->
                                                    <!-- ================ -->
                                                    <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Modern School">
                                    
                                                        <!-- main image -->
                                                        <img src="assets/images/modern-convent-1.JPG" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
                                    
                                                        <!-- Transparent Background -->
                                                        <!-- LAYER NR. 1 -->
<!--                                                        <div class="tp-caption sfb fadeout large_dark"
                                                             data-x="left"
                                                             data-y="220"
                                                             data-speed="500"
                                                             data-start="1000"
                                                             data-easing="easeOutQuad"><span id="newFont2"> Get Trained. Get Ahead</span>
                                                        </div>-->
                                                        <!-- LAYER NR. 2 -->
                                                        <!-- LAYER NR. 3 -->
                                                    </li>
                                                    <!-- slide 1 end -->
                                                    <!-- slide 2 start -->
                                                    <!-- ================ -->
                                                    <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Modern School">
                                    
                                                        <!-- main image -->
                                                        <img src="assets/images/modern-convent-2.JPG" alt="slidebg2" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
                                    
                                                        <!-- Transparent Background -->
<!--                                                        <div class="tp-caption dark-translucent-bg"
                                                             data-x="center"
                                                             data-y="bottom"
                                                             data-speed="600"
                                                             data-start="0">
                                                        </div>-->
<!--                                                        <div class="tp-caption sfb fadeout large_white"
                                                             data-x="center"
                                                             data-y="280"
                                                             data-speed="500"
                                                             data-start="1000"
                                                             data-easing="easeOutQuad"><span id="newFont2">Helping people grow their careers. Every day!</span>
                                                        </div>-->
                                    
                                    
                                                    </li>
                                                    <!-- slide 2 end -->
                                                    <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Modern School">
                                    
                                                        <!-- main image -->
                                                        <img src="assets/images/modern-convent-3.JPG" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
                                    
                                                        <!-- Transparent Background -->
                                                        <!-- LAYER NR. 1 -->
<!--                                                        <div class="tp-caption sfb fadeout large_dark"
                                                             data-x="left"
                                                             data-y="220"
                                                             data-speed="500"
                                                             data-start="1000"
                                                             data-easing="easeOutQuad"><span id="newFont2"> Get Trained. Get Ahead</span>
                                                        </div>-->
                                                        <!-- LAYER NR. 2 -->
                                                        <!-- LAYER NR. 3 -->
                                                    </li>
                                                    <li data-transition="random" data-slotamount="7" data-masterspeed="500" data-saveperformance="on" data-title="Modern School">
                                    
                                                        <!-- main image -->
                                                        <img src="assets/images/modern-convent-4.JPG" alt="slidebg1" data-bgposition="center top"  data-bgrepeat="no-repeat" data-bgfit="cover">
                                    
                                                        <!-- Transparent Background -->
                                                        <!-- LAYER NR. 1 -->
<!--                                                        <div class="tp-caption sfb fadeout large_dark"
                                                             data-x="left"
                                                             data-y="220"
                                                             data-speed="500"
                                                             data-start="1000"
                                                             data-easing="easeOutQuad"><span id="newFont2"> Get Trained. Get Ahead</span>
                                                        </div>-->
                                                        <!-- LAYER NR. 2 -->
                                                        <!-- LAYER NR. 3 -->
                                                    </li>
                                                </ul>
                                                <div class="tp-bannertimer"></div>
                                            </div>
                                        </div>
                                        <!-- slider revolution end -->
                                    
                                    </div>
                                    <!-- slideshow end -->
                                    
                                </div>
				<!-- slideshow end -->

			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>
                        <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
<!--                                    <p id="para-font">&nbsp;
                                        <b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>
                                     </p>-->
                                </div>
                            <div class="col-md-10">

                                   <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                                       <a href = "assets/pdf/admission-form.pdf" target="_blank" style="color:black;font-weight:bold;">Admissions open for the academic year 2017-18 in Modern Group of Schools.<span style="color:yellow">Click here to Download Form </span>For details contact<span style="color:#f1fa00;font-weight:bold" >
                                               <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '7566931851, 9713003663';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '7049923643, 9926740041'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '9926740041'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                              
                                           
                                           </span> . Hurry! Only few seats left.</a>
                                  </marquee>
                            </div>
                            </div>
                             
                        </div>
			<!-- section start -->
			<!-- ================ -->
                           <div class="clearfix"></div>
			<!-- section end -->
                        <!-- section start -->
			<!-- ================ -->
                          <section class="section clearfix" id="homeRow3">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<h3 id="heading-font">Latest <span class="text-default">Highlights</span></h3>
							<div class="separator-2"></div>
							<div class="block">
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" id="notificationMarquee"></marquee>   
                                                            <div class="text-right space-top">
                                                            </div>
							
							</div>
						</div>
						<div class="col-md-3">
							<h3 id="heading-font">Notice <span class="text-default">Board</span></h3>
							<div class="separator-2"></div>
                                                        <div class="block">
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" id="noticeboardMarquee">
                                                                
                                                                
                                                            </marquee>   
                                                            <div class="text-right space-top">
											<!--<a href="news.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>-->	
                                                            </div>
							
							</div>
							<!-- accordion start -->
							<!-- ================ -->
							
							<!-- accordion end -->
						</div>
                                            <div class="col-md-3">
							<h3 id="heading-font">Latest <span class="text-default">News</span></h3>
							<div class="separator-2"></div>
							<div class="block">
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" id="newsMarquee">
                                                                 
                                                                
                                                            </marquee>   
                                                            <div class="text-right space-top">
											<!--<a href="news.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>-->	
								</div>
							
							</div>
						</div>
                                            <div class="col-md-3">
							<h3 id="heading-font">Our <span class="text-default">Pride</span></h3>
							<div class="separator-2"></div>
                                                        <div class="block">
                                                             <?php
                                                        if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];
                                                             switch ($scat) {
                                                        case "1": ?>
                                                            <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" >
                                                                 
                                                                <div class="media margin-clear" id="newsAlert-wrap"><div class="media-left" id="newsAlert-left3"><div class="testimonial-image"><img src="server/controller/upload/1491579663.png" style="height:70px;width:70px;border: 5px solid #1da913;border-radius:50%;margin-left:10px;" alt="" title="" class="img-circle"></div></div><div class="media-body"><h5 class="" id="heading-font">Data Uploaded Soon</h5></div><div class="separator-2"></div></div>
                              
                                                            </marquee>    
                                                            <?php   break;
                                                        case "2": ?>
                                                                <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;" >
                                                                 
                                                                <div class="media margin-clear" id="newsAlert-wrap"><div class="media-left" id="newsAlert-left3"><div class="testimonial-image"><img src="server/controller/upload/1491579663.png" style="height:70px;width:70px;border: 5px solid #1da913;border-radius:50%;margin-left:10px;" alt="" title="" class="img-circle"></div></div><div class="media-body"><h5 class="" id="heading-font">Data Uploaded Soon</h5></div><div class="separator-2"></div></div>
                              
                                                            </marquee>  
                                                    <?php    break;
                                                        case "3": ?>	
							  <marquee behavior="scroll" direction="up" scrolldelay="200" onmouseover="this.stop();" onmouseout="this.start();" style="height:300px;"  id="selectionsMarquee">
                                                               
                                                                </marquee> 
                                                              <?php    break;
                                                       
                                                        
                                                        }
                                                      
                                                        }?>
								<div class="text-right space-top">
											<!--<a href="news.php" class="link-dark"><i class="fa fa-plus-circle pl-5 pr-5"></i>More</a>-->	
								</div>
							
							</div>

						</div>
                                       
						
					</div>
				</div>
			</section>
<!--                        <section class="section clearfix" id="homeRow1" style="box-shadow:inset 1px 1px 10px #000;">
				<div class="container">
					<div class="row">
						    <div class="col-md-6">
							<h3 id="heading-font">Thought of the <span class="text-default">Day</span></h3>
							<div class="separator-2"></div>
                                                       
                                                           
                                                                <div  id="qDay">
                                                                    <blockquote class="margin-clear">
                                                                        <h5 id="heading-font" class="text-justify" >"By the study of different religions we find that in essence they are one. –"</h5>
                                                                        <small style="float:left;font-weight:bold">Swami Vivekananda</small><br/><br/>
                                                                        <small style="float:left;">Posted By</small>
                                                                        <br/>
                                                                        <div id="tposted">
                                                                            <img src="server/controller/upload/1490341951.png" height="50" width="50" id="thought-image"/>
                                                                            <p style="position:relative;left:10px;top:10px;font-weight:bold;color:#000">Lalit Pastor</p>
                                                                        </div>
                                                                       
                                                                            
                                                                         
                                                                    </blockquote>
                                                                    
                                                                   
                                                                </div>
                                                                
                                                           
                                                  
                                                       
                                                        
							 accordion start 
							 ================ 
                                                        <p>&nbsp;</p>
                                                        <?php   
//                                                            if(isset($_SESSION['user_id'])){
//                                                             if($_SESSION['user_type']==='normal'){ ?>
                                                                <a href="registration_form.php" class="btn square btn-default btn-lg" ><b>Class Registration form</b></a>
                                                            <?php //     } 
                                                            
//                                                             }else{ ?>
                                                                  <button href="#" class="btn square btn-default btn-lg" data-toggle="modal" data-target="#demoRegistration"><b>Registration for Demo</b></button>
                                                         <?php //    } 
                                                            ?>
                                                        
                                                         
							 accordion end 
						</div>
						    <div class="col-md-6">
							<h3 id="heading-font">Question of the <span class="text-default">Day</span></h3>
							<div class="separator-2"></div>
                                                        <div  id="qDay">
                                                            <blockquote class="margin-clear">
                                                                <h5 id="heading-font">If one number of a Pythagorean triplet is 16, find the other two numbers ?</h5>
                                                                
                                                            </blockquote>
                                                            
							
							</div>
							 accordion start 
							 ================ 
                                                        <p>&nbsp;</p>
                                                         
							 accordion end 
						</div>
                                            
                                     
                                       
						
					</div>
				</div>
			</section>-->
			<section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-8">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;line-height:36px;">
                                                                <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '<strong>Modern Convent School, Mandideep</strong>';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '<strong>Modern Convent School, Obedullaganj</strong>'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '<strong>Modern English Medium Higher Secondary School, Tamot</strong>'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                                        
                                                    
                                                    </h1>
                                                    <div class="separator"></div>
                                                    <p id="cpara">On behalf of our organisation, I am pleased to present our website. Our Organisation has completed its 14 years of existence. Our organization administrative arrangements are now inplace & functioning. From the very beginning we believe that development has to be holistic if we wish to break the vicious cycle of poverty and to bring real fruits to the must needy, public health, education, women empowerment and community development continue to be the prime areas of our work. We wish to bring a substantial change on the ground by focusing our energies in these sectors as well as building our understanding about related issues. We firmly believe that a conductive and productive environment can’t be achieved unless all these parameters of the development equation are suitably answered...<a href="about-us.php">[...]</a></p>
                                                        
                                                         <p>&nbsp;</p>
                                                        
						</div>
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="height:180px;box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                       <?php  if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];
                                                             switch ($scat) {
                                                        case "1": ?>
                                                        <img src="assets/images/modern-mandideep.jpg"   />
                                                        <a href="assets/images/modern-mandideep.jpg"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                        <?php   break;
                                                        case "2": ?>
                                                        <img src="assets/images/modern-obj.jpg"   />
                                                         <a href="assets/images/modern-obj.jpg"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                        <?php    break;
                                                        case "3": ?>
                                                         <img src="assets/images/modern-tamot1.jpg"   />
                                                         <a href="assets/images/modern-tamot.JPG"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                         <?php    break;
                                                       
                                                         }
                                                      
                                                        }?>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
			<!-- section end -->
                     
			
			<div class="clearfix"></div>
			<!-- section end -->
			 <section class="pv-30  padding-bottom-clear dark-translucent-bg " id="row-testimonials"style="background:url('assets/images/bg/slide_3_bg.jpg'); box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            <div class="space-bottom">
                                <div class="owl-carousel content-slider" id="SelTestimonials">
                                    <?php // include_once 'includes/db.php';
//                                    $sql = "select * from feedback";
//                                    $result = mysqli_query($conn, $sql);
                                    
//                                     while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                                    ?>
<!--                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="server/controller/<?php // echo $row['image']; ?>" style="height:70px;width:70px;" alt="" title="" class="img-circle">
                                                    </div>
                                                        
                                                    <div class="testimonial-body">
                                                        <blockquote>
                                                            <p id="cpara"><?php //  echo $row['message']; ?></p>
                                                        </blockquote>
                                                        <div class="cpara3"><?php //  echo $row['name']; ?></div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>-->
                               <?php //  }   ?>
                                  
                                </div>
                                    
                            </div>
                        </section> 
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

                <script type="text/javascript">
                    $(document).ready(function() {
                        onLoadNotifications();
                        onLoadNoticeboard();
                        onLoadSelSelections();
                        onLoadSelNews();
//                        onLoadSelTestimonials();
                    });
                </script>
                 <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/SelNotifications.js"></script>
                <script type="text/javascript" src="ajax/SelNoticeboard.js"></script>
                <script type="text/javascript" src="ajax/SelSelections.js"></script>
                <script type="text/javascript" src="ajax/SelNews.js"></script>
<!--                <script type="text/javascript" src="ajax/SelTestimonials.js"></script>-->
               
	</body>
</html>
