<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Admission Procedure </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
            <style>
                #fee_structure tr th{
                    font-weight:bold;
                }
                #fee_structure{
                    margin-bottom:0px;
                    border-bottom:5px solid #0057b9;
                }
                #fee_structure tr td{
                    font-weight:bold;
                }
                 #fee_structure tr th:last-child{
                    background:green;
                    color:white;
                }
                #fee_structure tr td:last-child{
                    background:grey;
                    color:white;
                }
                .feeWrap{
                    width:100%;
                    overflow-y:auto;
                }
            </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('admission-procedure')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <div class="container">
                                    <ol class="breadcrumb">
                                        <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.php">Home</a></li>
                                        <li class="active">Admission Procedure</li>
                                   </ol>
                                </div>
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
     <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
<!--                                    <p id="para-font">&nbsp;
                                        <b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>
                                     </p>-->
                                </div>
                            <div class="col-md-10">

                                   <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                                       <a href = "assets/pdf/admission-form.pdf" target="_blank" style="color:black;font-weight:bold;">Admissions open for the academic year 2017-18 in Modern Group of Schools.<span style="color:yellow">Click here to Download Form </span>For details contact<span style="color:#f1fa00;font-weight:bold" >
                                               <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '7566931851, 9713003663';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '7049923643, 9926740041'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '9926740041'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                              
                                           
                                           </span> . Hurry! Only few seats left.</a>
                                  </marquee>
                               </div>
                            </div>
                             
                        </div>
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-12">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Admission Procedure</strong> </h1>
                                                    <div class="separator"></div>
                                            </div>
                                    <p>&nbsp;</p>
                                 </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <p id="cpara">The School is pleased to announce / declare the admission process for the admission to classes  for the session 2017-18.</p>
                                        <p id="cpara">The details of admission against vacancies could be found out from the school office in the Month of April – June 2017.</p>
                                        
                                        <p id="cpara">In order that you may inform us of your wish to seek admission to our School, kindly log on to <b>moderngroupofschools.com </b>in and fill the registration form.</p>
                                        <p>&nbsp;</p>
                                        </div>
                                        <p>&nbsp;</p>
                                          <div class="col-md-12">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Document Required</strong> </h1>
                                                    <div class="separator"></div>
                                                      <ul class="list-icons col-md-6" style="font-weight:bold">
                                                                                    
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Birth Certificate</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Cast Certificate</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Samagra Id Student + Family</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Ration Card</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Voter Id Card</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Mark Sheet</li>
                                                                               <div class="separator-2"></div>
                                                                               
                                                                               
                                                                               
                                                  </ul>
                                                         <ul class="list-icons col-md-6" style="font-weight:bold">
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>TC</li>
                                                                               <div class="separator-2"></div>
                                                                              
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Bank Passbook</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Aadhar Card</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>4 Passport Photo</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Domicile Certificate</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Income Certificate</li>
                                                                               <div class="separator-2"></div>
                                                                               
                                                  </ul>
                                            </div>
                                    </div>  
                                
                                      
                                        <p>&nbsp;</p>
                                          </div>
                                        <div class="container-fluid">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Fee Structure</strong> </h1>
                                                    <div class="separator"></div>
                                                    <div class="feeWrap" style="box-shadow: 4px  4px 10px rgba(0,0,0, 0.50);">
                                                                                                                <?php
                                                        if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];

                                                        switch ($scat) {
                                                        case "1": ?>
                                                                 <table class="table table-bordered table-colored table-hover " id="fee_structure">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Class</th>
										<th>Admission Fees</th>
                                                                                <th>School Fee<br><small style="font-size:11px">1st Installment</small></th>
                                                                                <th>School Fee<br><small style="font-size:11px">2nd Installment</small></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Nursery</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3250/-</td>
										<td>Rs. 3250/-</td>
									</tr>
									<tr>
										<td>2</td>
										<td>KG-1</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3250/-</td>
										<td>Rs. 3250/-</td>
									</tr>
									<tr>
										<td>3</td>
										<td>KG-2</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3400/-</td>
										<td>Rs. 3400/-</td>
									</tr>
									<tr>
										<td>4</td>
										<td>1st</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3400/-</td>
										<td>Rs. 3400/-</td>
									</tr>
									<tr>
										<td>5</td>
										<td>2nd</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3500/-</td>
										<td>Rs. 3500/-</td>
									</tr>
									<tr>
										<td>6</td>
										<td>3rd</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3500/-</td>
										<td>Rs. 3500/-</td>
									</tr>
									<tr>
										<td>7</td>
										<td>4th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3600/-</td>
										<td>Rs. 3600/-</td>
									</tr>
									<tr>
										<td>8</td>
										<td>5th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3600/-</td>
										<td>Rs. 3600/-</td>
									</tr>
									<tr>
										<td>9</td>
										<td>6th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3750/-</td>
										<td>Rs. 3750/-</td>
									</tr>
									<tr>
										<td>10</td>
										<td>7th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3750/-</td>
										<td>Rs. 3750/-</td>
									</tr>
									<tr>
										<td>11</td>
										<td>8th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 3750/-</td>
										<td>Rs. 3750/-</td>
									</tr>
									
									
								</tbody>
							</table>   
                                                     <?php   break;
                                                        case "2": ?>
                                                         <table class="table table-bordered table-colored table-hover " id="fee_structure">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Class</th>
										<th>Admission Fees<br><small style="font-size:11px">Development charge,sports Fees,Cultural Activity</small></th>
										<th>Registration fees</th>
										<th>Caution Money</th>
										<th>Examination Fees</th>
										<th>Diary+I Card</th>
                                                                                <th>Tution Fees</th>
                                                                                <th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Nursery</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Nil</td>
										<td>Rs. 100/-</td>
										<td>Rs. 9600/-</td>
										<td>Rs. 11700/-</td>
									</tr>
									<tr>
										<td>2</td>
										<td>KG-1</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Nil</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 11700/-</td>
                                                                                
									</tr>
									<tr>
										<td>3</td>
										<td>KG-2</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Nil</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 11700/-</td>
									</tr>
									<tr>
										<td>4</td>
										<td>1st</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9400/-</td>
                                                                                <td>Rs. 11800/-</td>
									</tr>
									<tr>
										<td>5</td>
										<td>2nd</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9400/-</td>
                                                                                <td>Rs. 11800/-</td>
									</tr>
									<tr>
										<td>6</td>
										<td>3rd</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9400/-</td>
                                                                                <td>Rs. 11800/-</td>
									</tr>
									<tr>
										<td>7</td>
										<td>4th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9200/-</td>
                                                                                <td>Rs. 11600/-</td>
									</tr>
									<tr>
										<td>8</td>
										<td>5th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9200/-</td>
                                                                                <td>Rs. 11600/-</td>
									</tr>
									<tr>
										<td>9</td>
										<td>6th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 12000/-</td>
									</tr>
									<tr>
										<td>10</td>
										<td>7th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 12000/-</td>
									</tr>
									<tr>
										<td>11</td>
										<td>8th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 12000/-</td>
									</tr>
									
									
								</tbody>
							</table>  
                                                    
                                                    <?php    break;
                                                        case "3": ?>
                                                         <h4 class="text-center " id="heading-font" style="text-transform:none;"><strong>Data Uploaded Soon</strong> </h4>
                                                    <div class="separator"></div>
<!--                                                         <table class="table table-bordered table-colored table-hover " id="fee_structure">
								<thead>
									<tr>
										<th>S.No.</th>
										<th>Class</th>
										<th>Admission Fees<br><small style="font-size:11px">Development charge,sports Fees,Cultural Activity</small></th>
										<th>Registration fees</th>
										<th>Caution Money</th>
										<th>Examination Fees</th>
										<th>Diary+I Card</th>
                                                                                <th>Tution Fees</th>
                                                                                <th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Nursery</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Nil</td>
										<td>Rs. 100/-</td>
										<td>Rs. 9600/-</td>
										<td>Rs. 11700/-</td>
									</tr>
									<tr>
										<td>2</td>
										<td>KG-1</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Nil</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 11700/-</td>
                                                                                
									</tr>
									<tr>
										<td>3</td>
										<td>KG-2</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Nil</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 11700/-</td>
									</tr>
									<tr>
										<td>4</td>
										<td>1st</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9400/-</td>
                                                                                <td>Rs. 11800/-</td>
									</tr>
									<tr>
										<td>5</td>
										<td>2nd</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9400/-</td>
                                                                                <td>Rs. 11800/-</td>
									</tr>
									<tr>
										<td>6</td>
										<td>3rd</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9400/-</td>
                                                                                <td>Rs. 11800/-</td>
									</tr>
									<tr>
										<td>7</td>
										<td>4th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9200/-</td>
                                                                                <td>Rs. 11600/-</td>
									</tr>
									<tr>
										<td>8</td>
										<td>5th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9200/-</td>
                                                                                <td>Rs. 11600/-</td>
									</tr>
									<tr>
										<td>9</td>
										<td>6th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 12000/-</td>
									</tr>
									<tr>
										<td>10</td>
										<td>7th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 12000/-</td>
									</tr>
									<tr>
										<td>11</td>
										<td>8th</td>
										<td>Rs. 500/-</td>
										<td>Rs. 500/-</td>
										<td>Rs. 1000/-</td>
										<td>Rs. 300/-</td>
                                                                                <td>Rs. 100/-</td>
                                                                                <td>Rs. 9600/-</td>
                                                                                <td>Rs. 12000/-</td>
									</tr>
									
									
								</tbody>
							</table>  -->
                                                    
                                                    <?php    break;
                                                       
                                                        
                                                        }
                                                        }   ?>
                                          
                                     </div>
                                            </div>
                                      
                                      
                                          
					
                                    
                                  
                                    
                               
                               
                        </section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/SelDownloads.js"></script>
                <script type="text/javascript">
                   onLoadDownloads();
                </script>
	</body>
</html>
