 
  <?php include 'includes/session.php'; ?>    
 <?php

    if(isset($_SESSION['scat'])){
        unset($_SESSION['scat']);
        header('Location:home.php');
    }
 
?> 
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
    <head>
        <meta charset="utf-8">
        <title>Modern Group of Schools</title>
            <meta name="description" content="As an Educational Organisation we are working since from 2002 and day by day we are enhancing our ability, work area and of course the way of serving people. Presently we have 06 authorized VTPs (approved by NCVT), 118 STAR & PMKVY Centers in all over India and 03 Schools.">
            <meta name="author" content="Infopark">
            <meta name="keywords" content="Best School in Bhopal, Mandideep, Tamot, ObedullaGanj">
            <meta name="author" content="Infopark">
        <!-- Mobile Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include 'includes/csslinks.php';?>
        <link href="homeanimation/css/eerie.css" rel="stylesheet" type="text/css" media="all"/>
        <script type="text/javascript">
            function scat(id)
            {  window.location.href='index.php?scat='+id;
//                alert(id);
             
            }
           
    </script>
        <style>
            html {
                position: relative;
                min-height: 100%;
            }
            .carousel-fade .carousel-inner .item {
                opacity: 0;
                transition-property: opacity;
            }
            .carousel-fade .carousel-inner .active {
                opacity: 1;
            }
            .carousel-fade .carousel-inner .active.left,
            .carousel-fade .carousel-inner .active.right {
                left: 0;
                opacity: 0;
                z-index: 1;
            }
            .carousel-fade .carousel-inner .next.left,
            .carousel-fade .carousel-inner .prev.right {
                opacity: 1;
            }
            .carousel-fade .carousel-control {
                z-index: 2;
            }
            @media all and (transform-3d),
            (-webkit-transform-3d) {
                .carousel-fade .carousel-inner > .item.next,
                .carousel-fade .carousel-inner > .item.active.right {
                    opacity: 0;
                    -webkit-transform: translate3d(0, 0, 0);
                    transform: translate3d(0, 0, 0);
                }
                .carousel-fade .carousel-inner > .item.prev,
                .carousel-fade .carousel-inner > .item.active.left {
                    opacity: 0;
                    -webkit-transform: translate3d(0, 0, 0);
                    transform: translate3d(0, 0, 0);
                }
                .carousel-fade .carousel-inner > .item.next.left,
                .carousel-fade .carousel-inner > .item.prev.right,
                .carousel-fade .carousel-inner > .item.active {
                    opacity: 1;
                    -webkit-transform: translate3d(0, 0, 0);
                    transform: translate3d(0, 0, 0);
                }
            }
            .item:nth-child(1) {
                background: url(assets/images/modern-school1.jpg) no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            .item:nth-child(2) {
                background: url(assets/images/modern-school2.jpg) no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
/*            .item:nth-child(3) {
                background: url() no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }*/
            .carousel {
                z-index: -99;
            }
            .carousel .item {
                position: fixed;
                width: 100%;
                height: 100%;
            }
            .title {
                text-align: center;
                margin-top: 20px;
                padding: 10px;
                text-shadow: 2px 2px #000;
                color: #FFF;
            }
            #hLogo{margin-left:auto;margin-right:auto;height:250px;width:250px; }
            #wrapper-home{
                background:rgba(0,0,0,0.5);
                min-height:100%;
                width:100%;
                
            }
            .overlay {
                height: 100%;
                width: 0;
                position: fixed;
                z-index: 1;
                top: 0;
                left: 0;
                background:#fe9500;
                background: -webkit-radial-gradient(#fe9500 ,#fd7801 99%); /* Safari 5.1-6.0 */
                background: -o-radial-gradient(#fe9500,#fd7801 99%); /* For Opera 11.6-12.0 */
                background: -moz-radial-gradient(#fe9500,#fd7801 99%); /* For Firefox 3.6-15 */
                background: radial-gradient(#fe9500,#fd7801 99%); /* Standard syntax */
                overflow-x: hidden;
                transition: 0.5s;
            }
            
            .overlay-content {
                position: relative;
                top:5%;
                width: 100%;
                text-align: center;
                margin-top: 30px;
            }
            
            .overlay a {
                padding: 8px;
                text-decoration: none;
                font-size: 36px;
                color:white;
                font-family:century gothic;
                display: block;
                transition: 0.3s;
            }
            
            .overlay a:hover, .overlay a:focus {
                color:#0088cc;
            }
            
            .overlay .closebtn {
                
                position: absolute;
                top: 20px;
                right: 45px;
                font-size:35px;
                cursor:pointer;
                color:white;
                font-family:century gothic;
                font-weight:bold;
                text-shadow:2px 2px #000;
            }
            .overlay .closebtn:hover{
                color:#000;
                text-shadow:2px 2px #ffffff;
            }
            @media screen and (max-height: 450px) {
                .overlay a {font-size: 20px}
                
                .overlay .closebtn {
                    
                    font-weight:bold;
                    
                    top: 15px;
                    right: 35px;
                    cursor:pointer;
                }
                #hbox h3{
                    position:relative;
                    top:-10px;
                    font-size:16px;
                    line-height:16px;
                }
               
            }
            #homeNav{
                font-size:30px;cursor:pointer;
                position:fixed;
                right:200px;
                top:100px;
                color:white;
                font-weight:bold;
                text-shadow:0px 4px 3px #000;
            }
            #homeNav:hover{
                color:#ffca07;
                
            }
            #webhead {
                color:white;
                text-align: center;
                font-size:100px;
                text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0,0,0,.1), 0 0 5px rgba(0,0,0,.1), 0 1px 3px rgba(0,0,0,.3), 0 3px 5px rgba(0,0,0,.2), 0 5px 10px rgba(0,0,0,.25), 0 10px 10px rgba(0,0,0,.2), 0 20px 20px rgba(0,0,0,.15);
            }
            #overlayLink{
                width:90%;
                margin-left:auto;
                margin-right:auto;
                
            }
            #overlayLink a{
                font-family:century gothic;
                font-weight:bold;
               
                
            }
            #hbox{
                top:30px;
                position: relative;
                -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
                -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
                box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
                }

  

#hbox:before, #hbox:after
{
 content:"";
    position:absolute;
    z-index:-1;
    -webkit-box-shadow:0 0 20px rgba(0,0,0,0.8);
    -moz-box-shadow:0 0 20px rgba(0,0,0,0.8);
    box-shadow:0 0 20px rgba(0,0,0,0.8);
    top:50%;
    bottom:0;
    left:10px;
    right:10px;
    -moz-border-radius:100px / 10px;
    border-radius:100px / 10px;
}
#hbox:after
{
 right:10px;
    left:auto;
    -webkit-transform:skew(8deg) rotate(3deg);
       -moz-transform:skew(8deg) rotate(3deg);
        -ms-transform:skew(8deg) rotate(3deg);
         -o-transform:skew(8deg) rotate(3deg);
            transform:skew(8deg) rotate(3deg);
}
#hbox h3{
    font-family:century gothic;
    line-height:26px;
    color:white;
    font-weight:bold;
}
#hbox a{
    font-family:century gothic;
    font-size:12px;
    
    font-weight:bold;
}
        </style>
    </head>
    <!-- body classes:  -->
    <!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
    <!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
    <!-- "transparent-header": makes the header transparent and pulls the banner to top -->
    <body class="no-trans  transparent-header ">
        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
        <!-- page wrapper start -->
        <!-- ================ -->
        <!-- Inspired by http://codepen.io/transportedman/pen/NPWRGq -->
        
        <div class="carousel slide carousel-fade" data-ride="carousel">
            
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                </div>
                <div class="item">
                </div>
               
            </div>
        </div>
        
        <!-- Remeber to put all the content you want on top of the slider below the slider code -->
        <div class="container-fluid">
           
            <span id="homeNav" onclick="openNav()" class="object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="1000">&#9776; Enter to the Site</span>
            <div id="wrapper-home">
                <img src="assets/images/logomoderngroup2.png" id="hLogo" class="object-non-visible animated object-visible zoomInDown" data-animation-effect="zoomInDown" data-effect-delay="800"/>
                <h1 id="webhead" class="mid animated fadeInUp mb30 medium-heading fw700 ">Modern Group of Schools</h1>
            </div>
           
        </div>
        <div id="myNav" class="overlay">
             <canvas id="canvas" class="bit"></canvas>
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">X</a>
            <div class="overlay-content" id="overlayLink">
                
                <img src="assets/images/logomoderngroup2.png" style="height:150px;width:150px" id="hLogo"/>
                <div class="row">
                      <div class="col-sm-4">
                        <div class="image-box  text-center mb-20" id="hbox">
                            <div class="overlay-container">
                                <img src="assets/images/modern-mandideep.jpg" alt="">
                                <div class="overlay-top">
                                    <div class="text">
                                        <h3>Modern Convent School, Mandideep</h3>
                                       
                                    </div>
                                </div>
                                <div class="overlay-bottom">
                                    <div class="links">
                                        <div class="col-sm-6 col-sm-offset-3"><a href="#" onclick="scat('1')" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                      <div class="col-sm-4" >
                        <div class="image-box  text-center mb-20" id="hbox">
                            <div class="overlay-container">
                                <img src="assets/images/modern-obj.jpg" alt="">
                                <div class="overlay-top">
                                    <div class="text">
                                        <h3>Modern Convent School, Obedullaganj</h3>
                                       
                                    </div>
                                </div>
                                <div class="overlay-bottom">
                                    <div class="links">
                                        <div class="col-sm-6 col-sm-offset-3"><a href="#" onclick="scat('2')" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    <div class="col-sm-4" >
                        <div class="image-box  text-center mb-20" id="hbox">
                            <div class="overlay-container">
                                <img src="assets/images/modern-tamot1.jpg" alt="">
                                <div class="overlay-top">
                                    <div class="text">
                                        <h3>Modern English Medium Hr. School, Tamot</h3>
                                       
                                    </div>
                                </div>
                                <div class="overlay-bottom">
                                    <div class="links">
                                        <div class="col-sm-6 col-sm-offset-3"><a href="#" onclick="scat('3')" class="btn btn-gray-transparent btn-animated btn-sm">View Details <i class="pl-10 fa fa-arrow-right"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                </div>
               
                
            </div>
        </div>
        <!-- page-wrapper end -->
        
        
        <?php include 'includes/jslinks.php';?>
        
        
        
        <script>
            $('.carousel').carousel();
        </script>
        <script>
            function openNav() {
                document.getElementById("myNav").style.width = "100%";
            }

            function closeNav() {
                document.getElementById("myNav").style.width = "0%";
            }
        </script>
        <script>
        $(window).resize(function(){
//            var ws = $(document ).width();
             if($(document).width()<668){
           
            $("#homeNav").css({'position':'fixed','right':'20px','top':'10px'});
            $("#hLogo").css({'position':'relative','text-align':'center','left':'2px','top':'70px','height':'100px','width':'100px','margin-left':'auto','margin-right':'auto'});
            $("#webhead").css({'position':'relative','top':'50px','font-size':'28px','line-height':'36px','margin-bottom':'30px','text-align':'center'});
            $("#myNav").css({'font-size':'18px','line-height':'36px'});
            
       }else{
            $("#homeNav").css({'position':'fixed','right':'','top':''});
           $("#hLogo").css({'position':'relative','text-align':'center','left':'','top':'','height':'','width':'','margin-left':'','margin-right':''});
           $("#webhead").css({'position':'relative','top':'','font-size':'','line-height':'','margin-bottom':''});
           $("#myNav").css({'font-size':'','line-height':''});
        }
            
        }
        
    );
          if($(document).width()<668){
             
            $("#homeNav").css({'position':'fixed','right':'20px','top':'10px'});
            $("#hLogo").css({'position':'relative','text-align':'center','height':'100px','width':'100px','left':'2px','top':'70px','margin-left':'auto','margin-right':'auto'});
           $("#webhead").css({'position':'relative','top':'50px','font-size':'28px','line-height':'36px','margin-bottom':'30px'});
              $("#myNav").css({'font-size':'18px','line-height':'36px'});
           
       }else{
          $("#homeNav").css({'position':'fixed','right':'','top':''});
           $("#hLogo").css({'position':'relative','text-align':'center','left':'','top':'','height':'','width':'','margin-left':'','margin-right':''});
           $("#webhead").css({'position':'relative','top':'','font-size':'','line-height':'','margin-bottom':''});
           $("#myNav").css({'font-size':'','line-height':''});
            
       } 

    </script>
     <script src="homeanimation/js/jquery-1.11.3.min.js"></script>
      <script src="homeanimation/js/bootstrap.min.js"></script>
      <script src="homeanimation/js/animsition.min.js"></script>
      
      <script src="homeanimation/js/jquery.countdown.min.js"></script>
      <script src="homeanimation/js/photoswipe.min.js"></script>
      <script src="homeanimation/js/photoswipe-ui-default.min.js"></script>
      <script src="homeanimation/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="homeanimation/js/script.js"></script>
      <script src="homeanimation/js/bit.js"></script>
    </body>
</html>
