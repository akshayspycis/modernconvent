<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Download Section </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('facilities')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <div class="container">
                                    <ol class="breadcrumb">
                                        <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
                                        <li class="active">Campus Facilities</li>
                                   </ol>
                                </div>
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
     <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
<!--                                    <p id="para-font">&nbsp;
                                        <b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>
                                     </p>-->
                                </div>
                            <div class="col-md-10">

                                   <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                                       <a href = "assets/pdf/admission-form.pdf" target="_blank" style="color:black;font-weight:bold;">Admissions open for the academic year 2017-18 in Modern Group of Schools.<span style="color:yellow">Click here to Download Form </span>For details contact<span style="color:#f1fa00;font-weight:bold" >
                                               <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '7566931851, 9713003663';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '7049923643, 9926740041'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '9926740041'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                              
                                           
                                           </span> . Hurry! Only few seats left.</a>
                                  </marquee>
                            </div>
                            </div>
                             
                        </div>
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-12">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Campus Facilities</strong> </h1>
                                                    <div class="separator"></div>
                                            </div>
                                    <p>&nbsp;</p>
                                 </div>
                                <div class="row">
						<div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>CAMPUS :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">Modern Convent School campus include comprises of academic blocks, smart classrooms, sports fields, science, computer, language and subject laboratories, library, indoor sports complex, form functional clusters of care and safety.</p>
                                                           <ul class="list-icons col-md-6" style="font-weight:bold">
                                                                                                                                                     <?php
                                                        if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];

                                                        switch ($scat) {
                                                        case "1": ?>
                                                                 <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Smart Classes</li>
                                                                                <div class="separator-2"></div>
                                                                                
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Learn By Fun</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Well Equipped Classes</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Play Ground</li>
                                                                               <div class="separator-2"></div>
                                                                               <li><i class="fa fa-angle-double-right pr-10 text-default"></i>CCTV Camera Surveillance Area</li>
                                                                               <div class="separator-2"></div>
                                                                               <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Internet Facility</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Indoor Games</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Counseling</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Qualified Teachers</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Transport Facility</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Laboratories for Science & Humanities</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Library</li>
                                                                               <div class="separator-2"></div>
                                                                               
                                                     <?php   break;
                                                        case "2": ?>
                                                         
                                                                 <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Smart Classes</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>A.C. Class Rooms</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Learn By Fun</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Well Equipped Classes</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Play Ground</li>
                                                                               <div class="separator-2"></div>
                                                                               <li><i class="fa fa-angle-double-right pr-10 text-default"></i>CCTV Camera Surveillance Area</li>
                                                                               <div class="separator-2"></div>
                                                                               <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Internet Facility</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Indoor Games</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Counseling</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Qualified Teachers</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Transport Facility</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Laboratories for Science & Humanities</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Library</li>
                                                                               <div class="separator-2"></div>
                                                                               
                                                    
                                                    <?php    break;
                                                        case "3": ?>
                                                        
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Well Equipped Classes</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Play Ground</li>
                                                                               <div class="separator-2"></div>
                                                                               <li><i class="fa fa-angle-double-right pr-10 text-default"></i>CCTV Camera Surveillance Area</li>
                                                                               <div class="separator-2"></div>
                                                                               <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Internet Facility</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Indoor Games</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Counseling</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Qualified Teachers</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Transport Facility</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Laboratories for Science & Humanities</li>
                                                                               <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Library</li>
                                                                               <div class="separator-2"></div>
                                                                               
                                                    
                                                    <?php    break;
                                                       
                                                        
                                                        }
                                                        }   ?>   
                                                                               
                                                                               
                                                  </ul>
                                                </div>
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                    <div class="overlay-container overlay-visible" style="height:180px;box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                       <?php  if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];
                                                             switch ($scat) {
                                                        case "1": ?>
                                                        <img src="assets/images/modern-mandideep.jpg"   />
                                                        <a href="assets/images/modern-mandideep.jpg"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                        <?php   break;
                                                        case "2": ?>
                                                        <img src="assets/images/modern-obj.jpg"   />
                                                         <a href="assets/images/modern-obj.jpg"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                        <?php    break;
                                                        case "3": ?>
                                                         <img src="assets/images/modern-tamot1.jpg"   />
                                                         <a href="assets/images/modern-tamot.JPG"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                         <?php    break;
                                                       
                                                         }
                                                      
                                                        }?>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						
					</div>
                         
                                 </div>
                        </section>
<!--                               <section class="light-gray-bg pv-30 clearfix" id="homeRow2">
                            <div class="container" >
					
                                <div class="row">
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/indore.jpg" >
                                                        <a href="assets/images/indore.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
                                                <div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>Smart Classes :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">Most of the the classrooms are running on Smart Technology, to make teaching learning process more effective.</p>
                                                </div>
						
						
					</div>
                         
                                 </div>
                        </section>
                             <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					
                                <div class="row">
						
                                                <div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>AC CLASSROOMS :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">A lack of cooling in the classroom reduce the concentration of students and teachers both.so we facilitate AC Class rooms for children to improve their concentration on study. </p>
                                                </div>
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/library.jpg"  >
                                                        <a href="assets/images/library.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						
					</div>
                         
                                 </div>
                        </section>
                             <section class="light-gray-bg pv-30 clearfix" id="homeRow2">
                            <div class="container" >
					
                                <div class="row">
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/indore.jpg" >
                                                        <a href="assets/images/indore.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
                                                <div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>LEARN BY FUN</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">With the help of this technique, an all-round development in children is seen and they become highly responsible and cultured citizens. This technique also has a unique method to make the students self-dependent citizens of the nation who are concerned about the welfare of the society as well as the nation.</p>
                                                </div>
						
						
					</div>
                         
                                 </div>
                        </section>
                             <section class="light-gray-bg pv-30 clearfix" id="homeRow2">
                            <div class="container" >
					
                                <div class="row">
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/indore.jpg" >
                                                        <a href="assets/images/indore.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
                                                <div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>INDOOR GAMES  :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">Our 11 feet high indoor complex is well equipped to deal with the boundless energy of a school full of students not only for games but even for activities. We also have: Badminton courts, Table Tennis tables, Chess Boards & a multi-purpose hall with a stage that can host full-school assemblies as well as special cultural & sporting events.</p>
                                                </div>
						
						
					</div>
                         
                                 </div>
                        </section>
			    <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					
                                <div class="row">
						
                                                <div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>LIBRARY  :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">A source of continuous delight and discovery for students, our library is an exhilarating treasure trove of information. In addition to a wide range of fiction and activity books, the library is well stocked with syllabus and reference books needed. Simple to locate and even simpler to use are the individual sections for journals and audio and video CDs. Our e-library is the resource center equipped with computers, while an adjoining deck invites both silent reflection and animated brainstorming. Parents are welcome to help out in book selection and recommend reading and research material.</p>
                                                </div>
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/library.jpg"  >
                                                        <a href="assets/images/library.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						
					</div>
                         
                                 </div>
                        </section>
                           <section class="light-gray-bg pv-30 clearfix" id="homeRow2">
                            <div class="container" >
					
                                <div class="row">
						
                                                
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/transport.jpg" style="width:355px;">
                                                        <a href="assets/images/transport.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						<div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>TRANSPORT :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">The Modern Convent School operates a fleet of buses that ply on pre-determined routes. Naturally, we insist that you or someone you trust be there in person to drop off and pick up your child from the bus stop. Our buses are regularly inspected by Quality Controllers and each bus carries a staff member with an on-board phone.</p>
                                                </div>
					</div>
                         
                                 </div>
                        </section>
                           <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					
                                <div class="row">
						
                                                
						<div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>SPORTS :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">There are few things as joyful to behold as children at play. Also, a child's body needs to be exercised as much as the mind. We ensure that this happens in a systematic way and in a supervised environment. Thus, not only do we have the facilities for all major sports, we also have specialist coaches to make good use of the infrastructure. The school can boast of its sports facilities which are all state of the art and fit to host all national and international tournaments.</p>
                                                </div>
                                                
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/sports.jpg" >
                                                        <a href="assets/images/sports.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
					</div>
                         
                                 </div>
                        </section>
                           <section class="light-gray-bg pv-30 clearfix" id="homeRow2">
                            <div class="container" >
					
                                <div class="row">
						
                                                
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                        <img src="assets/images/counsling.jpg" >
                                                        <a href="assets/images/counsling.jpg" class="popup-img overlay-link"><i class="icon-plus-1"></i></a>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
                                    
                                                
						<div class="col-md-8">
                                                    <h5 class="" id="heading-font"><strong>COUNSELLING :</strong></h5>
                                                    <div class="separator"></div>
                                                         <p id="cpara">Counseling Cell of the school comprises of Samcara - The Career Counseling Cell and a Psychological counsellor to ensure emotional health of the students. While specific problems are individually addressed and resolved, the counsellor also provides guidance to parents and works towards achieving a harmonious working environment in school. Likewise, the career counselors empower you to make choices and career decisions based on understanding the science behind the whole process. They help and develop career planning and decision making, maximising career opportunities, generating awareness among students of senior school.</p>
                                                         <p id="cpara">Likewise, the career counselors empower you to make choices and career decisions based on understanding the science behind the whole process. They help and develop career planning and decision making, maximising career opportunities, generating awareness among students of senior school.</p>
                                                </div>
					</div>
                         
                                 </div>
                        </section>-->
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
           
                
	</body>
</html>
