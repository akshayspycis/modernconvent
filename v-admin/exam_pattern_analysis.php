<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Exam Pattern</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     
    <style>
        .box-title,.btn,.modal-title{
            font-weight:bold;
        }
        
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
            #data tr th, #data2 tr th,  #data3 tr th, #data4 tr th, #data5 tr th, #data6 tr th, #data7 tr th, #data8 tr th,#data9 tr th{
                background:green;
                color:white;
                font-family:verdana;
                font-size:13px;
            }
            #data tr td, #data2 tr td, #data3 tr td, #data4 tr td, #data5 tr td, #data6 tr td, #data7 tr td, #data8 tr td,#data9 tr td{
                font-family:verdana;
                font-size:12px;
                font-weight:bold;
                text-align: center;
                
            }
           
            #tblresponsive{
               height:400px;
               overflow-y:scroll;
               
            } 
            #preTablebox{
                background:#ccc;
            }
            #preTablebox h2{
                font-size:24px;
                font-family:verdana ;
            }
            #preExamTable tbody tr td{
                    font-weight:bold;
                    text-align:center;
                }
                #preExamTable thead tr th{
                    font-weight:bold;
                    text-align:center;
                }
                #thead{
                    background:green;
                    font-weight:bold;
                    text-align:center;
                    font-weight:bold;
                    color:white;
                }
                #examAnalysis-head{
                    position:relative;top:-16px;
                    font-weight:bold;
                    text-align:center;
                }
                #tfoot{
                    background:green;
                    color:white;
                    font-weight:bold;
                    text-align:center;
                }
                #DesExamTable tr th{
                    font-weight:bold;
                    text-align:center;
                }
                #DesExamTable tr td{
                    font-weight:bold;
                    text-align:center;
                }
    </style>
  </head>
  
  <body class="skin-yellow  skin-yellow  sidebar-mini sidebar-collapse ">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Exam Pattern Analysis</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-md-4">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Exams</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addExams">Add Exams</button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Name of Exam</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-4">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Exams Sub Categories</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addSubCat">Add Sub Category</button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data2" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Exam Name </th>
                        <th>Sub Cat</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
             <div class="col-md-4">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Exam Phases</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addExamPhase">Add Exam Phase</button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data3" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Exam Name </th>
                        <th>Sub Cat</th>
                        <th>Phase</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          <div class="row">
                <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Exam Sections</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addExamSections">Add Exam Section</button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data4" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Exam Name </th>
                        <th>Sub Cat</th>
                        <th>Phase</th>
                        <th>Section Name</th>
                        <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
           <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><b>Preliminary Exams</b></h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addPreExams"><b>Add Preliminary Exam</b></button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data5" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                       <th width="5%"rowspan="2"><span id="examAnalysis-head">S.No.</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Exam Name</span> </th>
                        <th rowspan="2"><span id="examAnalysis-head">Sub Cat</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Phase</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Section Name</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">No. of Questions</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Max. Marks</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Duration in Min.</span></th>
                       <th colspan="6">Category Wise <b style="color:yellow">2016 </b>Cut Off</th>
                       <th rowspan="2" style="text-align:center"><span id="examAnalysis-head">Year</span></th>
                       <th rowspan="2" style="text-align:center"><span id="examAnalysis-head">Action</span></th>
                      </tr>
                    <tr>
                    <th>GEN</th>
                    <th>OBC</th>
                    <th>SC</th>
                    <th>ST</th>
                    <th>PH</th>
                    <th>VH</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
       <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><b>Mains Exam</b></h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addMainsExam"><b>Add Mains Exam</b></button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive" id="tblresponsive"> 
                   <table id="data6" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                       <th width="5%"rowspan="2"><span id="examAnalysis-head">S.No.</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Exam Name</span> </th>
                        <th rowspan="2"><span id="examAnalysis-head">Sub Cat</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Phase</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Section Name</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">No. of Questions</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Max. Marks</span></th>
                        <th rowspan="2"><span id="examAnalysis-head">Duration in Min.</span></th>
                       <th colspan="6">Category Wise <b style="color:yellow">2016 </b>Cut Off</th>
                       <th rowspan="2" style="text-align:center"><span id="examAnalysis-head">Year</span></th>
                       <th rowspan="2" style="text-align:center"><span id="examAnalysis-head">Action</span></th>
                      </tr>
                    <tr>
                    <th>GEN</th>
                    <th>OBC</th>
                    <th>SC</th>
                    <th>ST</th>
                    <th>PH</th>
                    <th>VH</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
           <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><b>Discriptive Type Exam</b></h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addDiscriptiveTest"><b>Add Discriptive Type Exam</b></button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data7" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                           <th width="5%">S.No.</th>
                           <th>Exam Name</th>
                           <th>Sub Category</th>
                           <th>Exam Phase</th>
                           <th>Name of Paper </th>
                           <th>Type of Paper</th>
                           <th>Duration in Min.</th>
                           <th>Max. Marks</th>
                       <th  style="text-align:center">Year</th>
                       <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
           <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><b>Group Discussion</b></h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addGroupDiscussion"><b>Add Group Discussion</b></button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data8" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                       <th width="5%">S.No.</th>
                       <th>Exam Name</th>
                       <th>Sub Category</th>
                       <th>Exam Phase</th>
                        <th>Content</th>
                       <th  style="text-align:center">Year</th>
                       <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
          
           <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><b>Personal Interview</b></h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addPersonalInterview"><b>Add Personal Interview</b></button>
                     
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data9" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                       <th width="5%">S.No.</th>
                       <th>Exam Name</th>
                       <th>Sub Category</th>
                       <th>Exam Phase</th>
                        <th>Content</th>
                       <th  style="text-align:center">Year</th>
                       <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div>
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
     <!-- Modal TO Insert Exams -->
    <div class="modal fade" id="insExams" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Exams </h4>
                </div>
                <form id ="insertform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="Name">Exam Name</label>
                                                    <input type="text" class="form-control" name="exam_name" id="exam_name" placeholder="Name here">
                                                </div>
                                                <!-- /.form-group -->
            
            
            
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
      <!-- Modal TO Edit Exams -->
    <div class="modal fade" id="editExams" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Exams </h4>
                </div>
                <form id ="updateExams" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="Name">Exams Name</label>
                                        <input type="hidden" class="form-control" name="exam_id" id="exam_id" placeholder="Name here" value="">
                                        <input type="text" class="form-control" name="exam_name" id="exam_name" placeholder="Name here" value="">
                                    </div>
                                    <!-- /.form-group -->
            
                                </div><!-- /.col -->
                                <!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" id="edit2" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
       <!-- Modal TO Delete Exams -->
    <div class="modal fade" id="delExams" role="dialog">
        <div class="modal-dialog">
            
            <!-- Modal content-->
            <div class="modal-content">
            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Exams</h4>
                </div>
                <form id ="delExamsForm">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="hidden"  name="id" id="id" value="" class="form-control"/>
                                <input type="hidden"  name="path" id="path" value="" class="form-control"/>
                                <p id ="msg">Sure to want to delete ?</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
       <!-- Modal TO Insert Sub Cat Exams -->
   <div class="modal fade" id="insExamsSubCat" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Exams Sub Category </h4>
                </div>
                <form id ="insertExamSubCatform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <input type="text" class="form-control" name="exam_sub_cat_name" id="exam_sub_cat_name" placeholder="Name here">
                                                </div>
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
       
       
         <!-- Modal TO Insert Exams Phases -->
   <div class="modal fade" id="insExamsPhase" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Exams Phase </h4>
                </div>
                <form id ="insertExamSPhaseform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="phase">Phase Name</label>
                                                    <select class="form-control" name="exam_phase_name" id="exam_phase_name">
                                                        <option value="Preliminary Exams">Preliminary Exams</option>
                                                        <option value="Main Exams">Main Exams</option>
                                                        <option value="Discriptive Type Test">Discriptive Type Test</option>
                                                        <option value="Discriptive Type Test">Group Discussion</option>
                                                        <option value="Personal Interview">Personal Interview</option>
                                                    </select>                                           

                                                </div>
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
 <!-- Modal TO Success -->        
<div id="enquirysuccess2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;color:white">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success</h4>
      </div>
      <div class="modal-body" id="errormsg">
          <p id="msg" style="color:gray;font-family:verdana;font-size:14px;line-height:22px"> Inserted Successfully</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
        <!-- Modal TO Warning -->
    <div id="warningmsg2" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="background:orangered;color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Warning</h4>
                </div>
                <div class="modal-body" id="errormsg">
                    <p id="msg" style="color:#000;font-family:century gothic;font-size:18px;font-weight:bold"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        
        </div>
    </div>
         <!-- Modal TO Insert Exams Sections -->
   <div class="modal fade" id="insExamsSections" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Exams Section </h4>
                </div>
                <form id ="insertExamsSectionform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Phase</label>
                                                    <select class="form-control" name="selExamPhase" id="selExamPhase">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="Section">Exam Section Name</label>
                                                    <input type="text" class="form-control" name="exam_section_name" id="exam_section_name" placeholder="Section Name here">
                                                </div>
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
 <!-- Modal TO Success --> 
    <!-- Modal TO Insert Prelims Exams Sections -->
   <div class="modal fade" id="insPrelimsExam" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Preliminary Exams Section </h4>
                </div>
                <form id ="insertPrelimsExamform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Phase</label>
                                                    <select class="form-control" name="selExamPhase" id="selExamPhase">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="Section">Exam Section Name</label>
                                                     <select class="form-control" name="selExamSection" id="selExamSection">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                          <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="sel1">No.of Questions</label>
                                                      <input type="text" class="form-control" name="no_of_questions" id="no_of_questions">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="sel1">Max. Marks</label>
                                                     <input type="text" class="form-control" name="max_marks" id="max_marks" >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="Name">Duration</label>
                                                    <input type="text" class="form-control" name="duration" id="duration" >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="Name">Year</label>
                                                    <input type="text" class="form-control" name="year" id="year" >
                                                </div>
                                            </div>
                                             
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                        <div class="row">
                                             <div class="col-md-12">
                                                   <div class="form-group">
                                                        <label for="Name">Category Wise Cut Off</label>
                                                    </div>
                                              </div>
                                        </div>
                                          <div class="row">
                                            <div class="separator2"></div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">GEN</label>
                                                        <input type="text" class="form-control" name="gen" id="gen" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">OBC</label>
                                                        <input type="text" class="form-control" name="obc" id="obc" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">SC</label>
                                                        <input type="text" class="form-control" name="sc" id="sc" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">ST</label>
                                                        <input type="text" class="form-control" name="st" id="st" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">PH</label>
                                                        <input type="text" class="form-control" name="ph" id="ph" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">VH</label>
                                                        <input type="text" class="form-control" name="vh" id="vh" >
                                                    </div>
                                                </div>
                                                
                                                <!-- /.form-group -->
                                           
                                        </div>
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
      <!-- Modal TO Insert Mains Exams  -->
   <div class="modal fade" id="insMainsExam" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Mains Exam  </h4>
                </div>
                <form id ="insertMainsExamform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Phase</label>
                                                    <select class="form-control" name="selExamPhase" id="selExamPhase">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="Section">Exam Section Name</label>
                                                     <select class="form-control" name="selExamSection" id="selExamSection">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                          <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="sel1">No.of Questions</label>
                                                      <input type="text" class="form-control" name="no_of_questions" id="no_of_questions">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="sel1">Max. Marks</label>
                                                     <input type="text" class="form-control" name="max_marks" id="max_marks" >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="Name">Duration</label>
                                                    <input type="text" class="form-control" name="duration" id="duration" >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="Name">Year</label>
                                                    <input type="text" class="form-control" name="year" id="year" >
                                                </div>
                                            </div>
                                             
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                        <div class="row">
                                             <div class="col-md-12">
                                                   <div class="form-group">
                                                        <label for="Name">Category Wise Cut Off</label>
                                                    </div>
                                              </div>
                                        </div>
                                          <div class="row">
                                            <div class="separator2"></div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">GEN</label>
                                                        <input type="text" class="form-control" name="gen" id="gen" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">OBC</label>
                                                        <input type="text" class="form-control" name="obc" id="obc" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">SC</label>
                                                        <input type="text" class="form-control" name="sc" id="sc" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">ST</label>
                                                        <input type="text" class="form-control" name="st" id="st" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">PH</label>
                                                        <input type="text" class="form-control" name="ph" id="ph" >
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label for="Name">VH</label>
                                                        <input type="text" class="form-control" name="vh" id="vh" >
                                                    </div>
                                                </div>
                                                
                                                <!-- /.form-group -->
                                           
                                        </div>
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
     <!-- Modal TO Insert DiscriptiveTest Sections -->
   <div class="modal fade" id="insDiscriptiveTest" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Discriptive Test </h4>
                </div>
                <form id ="insertDiscriptiveTestform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                       <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Phase</label>
                                                    <select class="form-control" name="selExamPhase" id="selExamPhase">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Paper Name</label>
                                                      <input type="text" class="form-control" name="papername" id="papername">
                                                </div>
                                                <!-- /.form-group -->
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div>
                                          <div class="row">
                                            <div class="col-md-6">
                                                
                                                 <div class="form-group">
                                                    <label for="Name">Duration</label>
                                                    <input type="text" class="form-control" name="duration" id="duration" >
                                                </div>
                                                <div class="form-group">
                                                    <label for="Name">Year</label>
                                                    <input type="text" class="form-control" name="year" id="year" >
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="papertype">papertype</label>
                                                     <input type="text" class="form-control" name="papertype" id="papertype" >
                                                </div>
                                                 <div class="form-group">
                                                    <label for="Name">Max. Marks</label>
                                                    <input type="text" class="form-control" name="maxmarks" id="maxmarks" >
                                                </div>
                                                
                                            </div>
                                           
                                             
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                      
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
       <!-- Modal TO Insert GD -->
   <div class="modal fade" id="insGroupDiscussion" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Group Discussion </h4>
                </div>
                <form id ="insertGroupDiscussionform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Phase</label>
                                                    <select class="form-control" name="selExamPhase" id="selExamPhase">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Year</label>
                                                    <input type="text" class="form-control" name="year" id="year">
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="Section">Group Discussion</label>
                                                    <textarea class="form-control" rows="5" name="content" id="content" style="resize:none" ></textarea>
                                                </div>
                                            </div><!-- /.col -->
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
           <!-- Modal TO Insert PI -->
   <div class="modal fade" id="insPersonalInterview" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Insert Personal Interview </h4>
                </div>
                <form id ="insertPersonalInterviewform" enctype="multipart/form-data">    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Name</label>
                                                    <select class="form-control" name="selExamName" id="selExamName">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Select Exam Phase</label>
                                                    <select class="form-control" name="selExamPhase" id="selExamPhase">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                </div>
                                                
                                                
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="Name">Exam Sub category</label>
                                                    <select class="form-control" name="selExamSubCat" id="selExamSubCat">
                                                        <option value="0">-Select-</option>
                                                    </select>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label for="sel1">Year</label>
                                                    <input type="text" class="form-control" name="year" id="year">
                                                </div>
                                                
                                              
                                              <!-- /.form-group -->
                                            </div><!-- /.col -->
                                              <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="Section">Content</label>
                                                        <textarea class="form-control" rows="5" name="content" id="content" style="resize:none" ></textarea>
                                                    </div>
                                                    
                                                </div>
                                            <!-- /.col -->
                                        </div><!-- /.row -->
                                    </div><!-- /.box-body -->
            
            
                                </div><!-- /.box -->
                            </div><!-- /.col -->
            
            
                        </div><!-- /.box -->
            
            
                        <div class="modal-footer">
                            <div class="validate pull-left" style="font-weight:bold;"></div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
  </body>
  <script>
      $(document).ready(function() {
          $("#addExams").click(function(){
            $("#insExams").modal('show');}); 
      });
         $("#insExams").on('shown.bs.modal', function(){
         });
          $('#insertform').submit(function() { 
                 if(this.exam_name.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Enter 'Name'.") ;
                      
                  });
                  this.exam_name.focus();
                  return false;
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsExams.php",
                      data:$('#insertform').serialize(),
                      success: function(data){
//                             alert(data.trim());
                          $('#enquirysuccess2').modal('show'); 
                          $("#enquirysuccess2").on('shown.bs.modal', function(){
                              //                        $('#msg').append(data) ;
                          }); 
                          setTimeout(function(){
                              $('#enquirysuccess2').modal('hide')
                          }, 10000);
                          $('#insertform').each(function(){
                              this.reset();
                              
                          });
                          onLoadExams();
                      }
                      
                  });
              }
              
              return false;
          });
  </script>
  <script>
      /* Select Exams */  
      $(document).ready(function() {
             exams ={};
             examssubcategory={};
             examsphases ={};
             examssections ={};
             prelimsexam ={};
             mainsexam ={};
             discriptivetest ={};
             groupdiscussion ={};
             onLoadExams();
             onLoadExamsSubCat();
             onLoadExamsphases();
             onLoadExamsSections();
             onLoadPrelimsExam();
             onLoadMainsExam();
             onLoadDiscriptiveTest();
             onLoadGroupDiscussion();
             onLoadPersonalInterview();
       });
      function onLoadExams(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExams.php",
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            exams[article.id]={};
                            exams[article.id]["exam_id"]=article.exam_id;
                            exams[article.id]["exam_name"]=article.exam_name;
                            $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exams_row'}).append("Edit").click(function (){edit_exams_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_exams_row'}).append("Delete").click(function (){delete_exams_row(article.exam_id); })))
                             );
                        });
                    }
                });   
             }
  </script>
  <script>
       function edit_exams_row(exam_id,exam_name){
                 $("#editExams").modal('show'); 
                 $('#edit2').off();
                  $("#updateExams").find("#exam_id").val(exam_id);  
                  $("#updateExams").find("#exam_name").val(exam_name);  
                  $('#updateExams').submit(function(){
                    
                      if(this.exam_name.value === ""){
                      $('#warningmsg2').modal('show'); 
                      $("#warningmsg2").on('shown.bs.modal', function(){
                       $("#errormsg").find("#msg").empty(); 
                          $("#errormsg").find("#msg").append("Please Enter 'Name'.") ;
                             });
                      this.exam_name.focus();
                      return false;
                  }else{
                       $.ajax({
                            type:"post",
                            url:"../server/controller/UpdExams.php",
                            data:$('#updateExams').serialize(),
                            success: function(data){
//                            alert(data.trim());
                            onLoadExams();
                            $("#editExams").modal('hide');; 
                      } 
                      
                      });
                  }
                        
                    
                    return false;
    });
      }
       function delete_exams_row(exam_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExams.php",
                            data:{"exam_id":exam_id},
                            success: function(data){
//                            alert(data.trim());
                            onLoadExams();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
  <!--For Sub Cat Start-->
   <script>
      $(document).ready(function() {
          $("#addSubCat").click(function(){
            $("#insExamsSubCat").modal('show');}); 
      });
         $("#insExamsSubCat").on('shown.bs.modal', function(){
         });
          $('#insertExamSubCatform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id});
                        $('#insertExamSubCatform').find('#selExamName').append(a);                    
                        });
                        }
                        }); 
              $('#insertExamSubCatform').submit(function() { 
                 if(this.exam_sub_cat_name.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Enter 'Name'.") ;
                      
                  });
                  this.exam_name.focus();
                  return false;
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsExamsSubCategory.php",
                      data:$('#insertExamSubCatform').serialize(),
                      success: function(data){
//                             alert(data.trim());
                          $('#enquirysuccess2').modal('show'); 
                          $("#enquirysuccess2").on('shown.bs.modal', function(){
                              //                        $('#msg').append(data) ;
                          }); 
                          setTimeout(function(){
                              $('#enquirysuccess2').modal('hide')
                          }, 10000);
                          $('#insertExamSubCatform').each(function(){
                              this.reset();
                              
                          });
                          onLoadExamsSubCat();
                      }
                      
                  });
              }
              
              return false;
          });
  </script>
  <script>
      /* Select Exams for Sub Cat */  
      function onLoadExamsSubCat(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExamsSubCategory.php",
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data2 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            examssubcategory[article.id]={};
                            examssubcategory[article.id]["exam_sub_cat_id"]=article.exam_sub_cat_id;
                            examssubcategory[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            examssubcategory[article.id]["exam_name"]=article.exam_name;
                            $("#data2").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
//                                .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_exam_sub_cat_row'}).append("Delete").click(function (){delete_exam_sub_cat_row(article.exam_sub_cat_id); })))
                             );
                        });
                    }
                });   
             }
  </script>
   <script>
      function delete_exam_sub_cat_row(exam_sub_cat_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExamsSubCategory.php",
                            data:{"exam_sub_cat_id":exam_sub_cat_id},
                            success: function(data){
//                            alert(data.trim());
                            onLoadExamsSubCat();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
   <!--For Sub Cat End-->
    <!--For Phases Start-->
  <script>
      $(document).ready(function() {
          $("#addExamPhase").click(function(){
            $("#insExamsPhase").modal('show');}); 
      });
         $("#insExamsPhase").on('shown.bs.modal', function(){
                
          });
                $('#insertExamSPhaseform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertExamSPhaseform').find('#selExamName').append(a);                    
                        });
                        $('#insertExamSPhaseform').find('#selExamName').trigger("change");
                        }
                        });
                
              
                 $('#insertExamSPhaseform').find('#selExamName').change(function()
                 {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertExamSPhaseform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertExamSPhaseform').find('#selExamSubCat').append(b);                    
                        });
                        }
                        });  
                        return false;
                });
             
              
                 $('#insertExamSPhaseform').submit(function() { 
                 if(this.selExamName.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Name'.") ;
                      
                  });
                  this.selExamName.focus();
                  return false;
              }
                 else if(this.selExamSubCat.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Sub Category'.") ;
                      
                  });
                  this.selExamSubCat.focus();
                  return false;
              }
                 else if(this.exam_phase_name.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Enter Exam Phase.") ;
                     
                  });
                    setTimeout(function(){
                    $('#warningmsg2').modal("hide");
                    }, 2000);
                  this.exam_phase_name.focus();
                  
                  return false;
                  
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsExamsPhases.php",
                      data:$('#insertExamSPhaseform').serialize(),
                      success: function(data){
//                             alert(data.trim());
                          $('#enquirysuccess2').modal('show'); 
                          $("#enquirysuccess2").on('shown.bs.modal', function(){
                              //                        $('#msg').append(data) ;
                          }); 
                          setTimeout(function(){
                              $('#enquirysuccess2').modal('hide')
                          }, 10000);
                          $('#insertExamSPhaseform').each(function(){
                              this.reset();
                              
                          });
                          onLoadExamsphases();
                      }
                      
                  });
              }
              
              return false;
          });
            
  </script>
   <script>
      /* Select Exams Phases */  
      function onLoadExamsphases(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExamsPhases.php",
                    success: function(data) {
//                       
                       var duce = jQuery.parseJSON(data);
                        $("#data3 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            examsphases[article.id]={};
                            examsphases[article.id]["exam_phase_id"]=article.exam_phase_id;
                            examsphases[article.id]["exam_name"]=article.exam_name;
                            examsphases[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            examsphases[article.id]["exam_phase_name"]=article.exam_phase_name;
                            $("#data3").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
//                              .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_examphases_row'}).append("Delete").click(function (){delete_examphases_row(article.exam_phase_id); })))
                             );
                        });
                    }
                });   
             }
  </script>  
     <script>
      function delete_examphases_row(exam_phase_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExamsPhases.php",
                            data:{"exam_phase_id":exam_phase_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadExamsphases();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
  
  <!--Exam Section Begin-->
  <script>
      $(document).ready(function() {
          $("#addExamSections").click(function(){
            $("#insExamsSections").modal('show');}); 
         });
         $("#insExamsSections").on('shown.bs.modal', function(){
                
          });
                $('#insertExamsSectionform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
//                        alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertExamsSectionform').find('#selExamName').append(a);                    
                        });
                        $('#insertExamsSectionform').find('#selExamName').trigger("change");
                        }
                        });
                  $('#insertExamsSectionform').find('#selExamName').change(function()
                  {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertExamsSectionform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertExamsSectionform').find('#selExamSubCat').append(b);                    
                        });
                        $('#insertExamsSectionform').find('#selExamSubCat').trigger("change");
                        }
                        });  
                        return false;
                });
                  $('#insertExamsSectionform').find('#selExamSubCat').change(function()
                  {
                 var exam_sub_cat_id=$(this).val();
                 var dataString = 'exam_sub_cat_id='+exam_sub_cat_id;
                 $('#insertExamsSectionform').find('#selExamPhase').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsPhases2.php",
                        data:{"exam_sub_cat_id":exam_sub_cat_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_phase_name).attr({'value':article.exam_phase_id});
                        $('#insertExamsSectionform').find('#selExamPhase').append(b);                    
                        });
                        }
                        });  
                        return false;
                });
                $('#insertExamsSectionform').submit(function() { 
                   if(this.selExamName.value === "") {
                       
                      $('#warningmsg2').modal('show'); 
                      $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Name'.") ;
                      
                  });
                  this.selExamName.focus();
                  return false;
              }
                 else if(this.selExamSubCat.value === "") {
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Sub Category'.") ;
                      
                  });
                  this.selExamSubCat.focus();
                  return false;
              }
                 else if(this.selExamPhase.value === ""){
                  $('#warningmsg2').modal('show'); 
                  $("#warningmsg2").on('shown.bs.modal', function(){
                      $("#errormsg").find("#msg").empty(); 
                      $("#errormsg").find("#msg").append("Please Select 'Exam Phase'.") ;
                   });
                  this.selExamPhase.focus();
                  return false;
              }else if(this.exam_section_name.value === "") {
                      alert("Please Enter Section Name");
                  this.exam_section_name.focus();
                  return false;
                  
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsExamsSections.php",
                      data:$('#insertExamsSectionform').serialize(),
                      success: function(data){
                             alert(data.trim());
                          $('#insertExamsSectionform').each(function(){
                              this.reset();
                              
                          });
                          onLoadExamsSections();
                      }
                      
                  });
              }
              
              return false;
          });
            
  </script>
     <script>
      /* Select Exams Sections */  
      function onLoadExamsSections(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelExamsSections.php",
                    data:{exam_phase_id:''},
                    success: function(data) {
//                       alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data4 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            examssections[article.id]={};
                            examssections[article.id]["exam_section_id"]=article.exam_section_id;
                            examssections[article.id]["exam_name"]=article.exam_name;
                            examssections[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            examssections[article.id]["exam_phase_name"]=article.exam_phase_name;
                            examssections[article.id]["exam_section_name"]=article.exam_section_name;
                            $("#data4").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
                                .append($('<td/>').html(article.exam_section_name))
//                              .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_examphases_row'}).append("Delete").click(function (){delete_examsections_row(article.exam_section_id); })))
                             );
                        });
                    }
                });   
             }
  </script>  
     <script>
      function delete_examsections_row(exam_section_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelExamsSections.php",
                            data:{"exam_section_id":exam_section_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadExamsSections();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
  
   <!--Prelims Exams  Begin-->
  <script>
      $(document).ready(function() {
          $("#addPreExams").click(function(){
            $("#insPrelimsExam").modal('show');}); 
         });
           $("#insPrelimsExam").on('shown.bs.modal', function(){
               $('#insertPrelimsExamform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
//                        alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertPrelimsExamform').find('#selExamName').append(a);                    
                        });
                        $('#insertPrelimsExamform').find('#selExamName').trigger("change");
                        }
                        });
              });
                
                  $('#insertPrelimsExamform').find('#selExamName').change(function()
                  {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertPrelimsExamform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertPrelimsExamform').find('#selExamSubCat').append(b);                    
                        });
                        $('#insertPrelimsExamform').find('#selExamSubCat').trigger("change");
                        }
                        });  
                        return false;
                });
                  $('#insertPrelimsExamform').find('#selExamSubCat').change(function()
                  {
                 var exam_sub_cat_id=$(this).val();
                 var dataString = 'exam_sub_cat_id='+exam_sub_cat_id;
                 $('#insertPrelimsExamform').find('#selExamPhase').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsPhases2.php",
                        data:{"exam_sub_cat_id":exam_sub_cat_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_phase_name).attr({'value':article.exam_phase_id});
                        $('#insertPrelimsExamform').find('#selExamPhase').append(b);                    
                        });
                         $('#insertPrelimsExamform').find('#selExamPhase').trigger("change");
                        }
                        });  
                        return false;
                });
                 $('#insertPrelimsExamform').find('#selExamPhase').change(function()
                  {
                 var exam_phase_id=$(this).val();

                 $('#insertPrelimsExamform').find('#selExamSection').empty();
               
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSections.php",
                        data:{'exam_phase_id':exam_phase_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_section_name).attr({'value':article.exam_section_id});
                        $('#insertPrelimsExamform').find('#selExamSection').append(b);                    
                        });
//                         $('#insertPrelimsExamform').find('#selExamPhase').trigger("change");
                        }
                        });  
                        return false;
                });
                $('#insertPrelimsExamform').submit(function() { 
                 if(this.no_of_questions.value === "") {
                      alert("Please Enter No. of Questions");
                  this.no_of_questions.focus();
                  return false;
               }
               else if(this.max_marks.value === "") {
                      alert("Please Enter Maximum Marks");
                  this.max_marks.focus();
                  return false;
              }
               else if(this.duration.value === "") {
                      alert("Please Enter Deuration in Minutes");
                  this.duration.focus();
                  return false;
              }
               else if(this.year.value === "") {
                      alert("Please Enter Year");
                  this.year.focus();
                  return false;
              }
               else if(this.gen.value === "") {
                      alert("Please Enter General Cut off");
                  this.gen.focus();
                  return false;
              }
               else if(this.obc.value === "") {
                       alert("Please Enter OBC Cut off");
                  this.obc.focus();
                  return false;
              }
               else if(this.sc.value === "") {
                       alert("Please Enter SC Cut off");
                  this.sc.focus();
                  return false;
              }
               else if(this.st.value === "") {
                       alert("Please Enter ST Cut off");
                  this.st.focus();
                  return false;
              }
               else if(this.ph.value === "") {
                       alert("Please Enter PH Cut off");
                  this.ph.focus();
                  return false;
              }
               else if(this.vh.value === "") {
                       alert("Please Enter VH Cut off");
                  this.vh.focus();
                  return false;
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsPrelimsExam.php",
                      data:$('#insertPrelimsExamform').serialize(),
                      success: function(data){
                             alert(data.trim());
                          $('#insertPrelimsExamform').each(function(){
                              this.reset();
                              
                          });
                          onLoadPrelimsExam();
                      }
                      
                  });
              }
              
              return false;
          });
            
  </script>
   <script>
      /* Select Prelims Exams */  
      function onLoadPrelimsExam(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelPrelimsExam.php",
                    data:{prelimsexam_id:''},
                    success: function(data) {
//                       alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data5 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            prelimsexam[article.id]={};
                            prelimsexam[article.id]["exam_name"]=article.exam_name;
                            prelimsexam[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            prelimsexam[article.id]["exam_phase_name"]=article.exam_phase_name;
                            prelimsexam[article.id]["exam_section_name"]=article.exam_section_name;
                            prelimsexam[article.id]["no_of_questions"]=article.no_of_questions;
                            prelimsexam[article.id]["max_marks"]=article.max_marks;
                            prelimsexam[article.id]["duration"]=article.duration;
                            prelimsexam[article.id]["gen"]=article.gen;
                            prelimsexam[article.id]["obc"]=article.obc;
                            prelimsexam[article.id]["sc"]=article.sc;
                            prelimsexam[article.id]["st"]=article.st;
                            prelimsexam[article.id]["ph"]=article.ph;
                            prelimsexam[article.id]["vh"]=article.vh;
                            prelimsexam[article.id]["year"]=article.year;
                            $("#data5").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
                                .append($('<td/>').html(article.exam_section_name))
                                .append($('<td/>').html(article.no_of_questions))
                                .append($('<td/>').html(article.max_marks))
                                .append($('<td/>').html(article.duration))
                                .append($('<td/>').html(article.gen))
                                .append($('<td/>').html(article.obc))
                                .append($('<td/>').html(article.sc))
                                .append($('<td/>').html(article.st))
                                .append($('<td/>').html(article.ph))
                                .append($('<td/>').html(article.vh))
                                .append($('<td/>').html(article.year))
                                
//                              .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_prelimsexam_row'}).append("Delete").click(function (){delete_prelimsexam_row(article.prelimsexam_id); })))
                             );
                        });
                    }
                });   
             }
  </script>
  
  
   <script>
       /* Delet Prelims Exams */  
      function delete_prelimsexam_row(prelimsexam_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelPrelimsExam.php",
                            data:{"prelimsexam_id":prelimsexam_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadPrelimsExam();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
   <!--Mains Exams  Begin-->
  <script>
      $(document).ready(function() {
          $("#addMainsExam").click(function(){
            $("#insMainsExam").modal('show');}); 
         });
           $("#insMainsExam").on('shown.bs.modal', function(){
               $('#insertMainsExamform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
//                        alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertMainsExamform').find('#selExamName').append(a);                    
                        });
                        $('#insertMainsExamform').find('#selExamName').trigger("change");
                        }
                        });
              });
                
                  $('#insertMainsExamform').find('#selExamName').change(function()
                  {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertMainsExamform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertMainsExamform').find('#selExamSubCat').append(b);                    
                        });
                        $('#insertMainsExamform').find('#selExamSubCat').trigger("change");
                        }
                        });  
                        return false;
                });
                  $('#insertMainsExamform').find('#selExamSubCat').change(function()
                  {
                 var exam_sub_cat_id=$(this).val();
                 var dataString = 'exam_sub_cat_id='+exam_sub_cat_id;
                 $('#insertMainsExamform').find('#selExamPhase').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsPhases2.php",
                        data:{"exam_sub_cat_id":exam_sub_cat_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_phase_name).attr({'value':article.exam_phase_id});
                        $('#insertMainsExamform').find('#selExamPhase').append(b);                    
                        });
                         $('#insertMainsExamform').find('#selExamPhase').trigger("change");
                        }
                        });  
                        return false;
                });
                 $('#insertMainsExamform').find('#selExamPhase').change(function()
                  {
                 var exam_phase_id=$(this).val();

                 $('#insertMainsExamform').find('#selExamSection').empty();
               
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSections.php",
                        data:{'exam_phase_id':exam_phase_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_section_name).attr({'value':article.exam_section_id});
                        $('#insertMainsExamform').find('#selExamSection').append(b);                    
                        });
//                         $('#insertMainsExamform').find('#selExamPhase').trigger("change");
                        }
                        });  
                        return false;
                });
                $('#insertMainsExamform').submit(function() { 
                 if(this.no_of_questions.value === "") {
                      alert("Please Enter No. of Questions");
                  this.no_of_questions.focus();
                  return false;
               }
               else if(this.max_marks.value === "") {
                      alert("Please Enter Maximum Marks");
                  this.max_marks.focus();
                  return false;
              }
               else if(this.duration.value === "") {
                      alert("Please Enter Deuration in Minutes");
                  this.duration.focus();
                  return false;
              }
               else if(this.year.value === "") {
                      alert("Please Enter Year");
                  this.year.focus();
                  return false;
              }
               else if(this.gen.value === "") {
                      alert("Please Enter General Cut off");
                  this.gen.focus();
                  return false;
              }
               else if(this.obc.value === "") {
                       alert("Please Enter OBC Cut off");
                  this.obc.focus();
                  return false;
              }
               else if(this.sc.value === "") {
                       alert("Please Enter SC Cut off");
                  this.sc.focus();
                  return false;
              }
               else if(this.st.value === "") {
                       alert("Please Enter ST Cut off");
                  this.st.focus();
                  return false;
              }
               else if(this.ph.value === "") {
                       alert("Please Enter PH Cut off");
                  this.ph.focus();
                  return false;
              }
               else if(this.vh.value === "") {
                       alert("Please Enter VH Cut off");
                  this.vh.focus();
                  return false;
              }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsMainsExam.php",
                      data:$('#insertMainsExamform').serialize(),
                      success: function(data){
                             alert(data.trim());
                          $('#insertMainsExamform').each(function(){
                              this.reset();
                              
                          });
                         onLoadMainsExam();
                      }
                      
                  });
              }
              
              return false;
          });
            
  </script>
   <script>
      /* Select Prelims Exams */  
      function onLoadMainsExam(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelMainsExam.php",
                    data:{mainsexam_id:''},
                    success: function(data) {
//                       alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data6 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            mainsexam[article.id]={};
                            mainsexam[article.id]["exam_name"]=article.exam_name;
                            mainsexam[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            mainsexam[article.id]["exam_phase_name"]=article.exam_phase_name;
                            mainsexam[article.id]["exam_section_name"]=article.exam_section_name;
                            mainsexam[article.id]["no_of_questions"]=article.no_of_questions;
                            mainsexam[article.id]["max_marks"]=article.max_marks;
                            mainsexam[article.id]["duration"]=article.duration;
                            mainsexam[article.id]["gen"]=article.gen;
                            mainsexam[article.id]["obc"]=article.obc;
                            mainsexam[article.id]["sc"]=article.sc;
                            mainsexam[article.id]["st"]=article.st;
                            mainsexam[article.id]["ph"]=article.ph;
                            mainsexam[article.id]["vh"]=article.vh;
                            mainsexam[article.id]["year"]=article.year;
                            $("#data6").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
                                .append($('<td/>').html(article.exam_section_name))
                                .append($('<td/>').html(article.no_of_questions))
                                .append($('<td/>').html(article.max_marks))
                                .append($('<td/>').html(article.duration))
                                .append($('<td/>').html(article.gen))
                                .append($('<td/>').html(article.obc))
                                .append($('<td/>').html(article.sc))
                                .append($('<td/>').html(article.st))
                                .append($('<td/>').html(article.ph))
                                .append($('<td/>').html(article.vh))
                                .append($('<td/>').html(article.year))
                                
//                              .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_mainsexam_row'}).append("Delete").click(function (){delete_mainsexam_row(article.mainsexam_id); })))
                             );
                        });
                    }
                });   
             }
  </script>
  
  <script>
       /* Delet Prelims Exams */  
      function delete_mainsexam_row(mainsexam_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelMainsExam.php",
                            data:{"mainsexam_id":mainsexam_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadMainsExam();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
   <script>
      $(document).ready(function() {
          $("#addDiscriptiveTest").click(function(){
            $("#insDiscriptiveTest").modal('show');}); 
      });
         $("#insDiscriptiveTest").on('shown.bs.modal', function(){
         });
          $('#insertDiscriptiveTestform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
//                        alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertDiscriptiveTestform').find('#selExamName').append(a);                    
                        });
                        $('#insertDiscriptiveTestform').find('#selExamName').trigger("change");
                        }
                        });
                  $('#insertDiscriptiveTestform').find('#selExamName').change(function()
                  {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertDiscriptiveTestform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertDiscriptiveTestform').find('#selExamSubCat').append(b);                    
                        });
                        $('#insertDiscriptiveTestform').find('#selExamSubCat').trigger("change");
                        }
                        });  
                        return false;
                });
                  $('#insertDiscriptiveTestform').find('#selExamSubCat').change(function()
                  {
                 var exam_sub_cat_id=$(this).val();
                 var dataString = 'exam_sub_cat_id='+exam_sub_cat_id;
                 $('#insertDiscriptiveTestform').find('#selExamPhase').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsPhases2.php",
                        data:{"exam_sub_cat_id":exam_sub_cat_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_phase_name).attr({'value':article.exam_phase_id});
                        $('#insertDiscriptiveTestform').find('#selExamPhase').append(b);                    
                        });
                        }
                        });  
                        return false;
                });
          $('#insertDiscriptiveTestform').submit(function() { 
                 if(this.papername.value === "") {
                      alert("Please Enter Paper Name");
                  this.papername.focus();
                  return false;
               }
               else if(this.papertype.value === "") {
                      alert("Please Enter Type of Paper");
                  this.papertype.focus();
                  return false;
              }
               else if(this.duration.value === "") {
                      alert("Please Enter Deuration in Minutes");
                  this.duration.focus();
                  return false;
              }
              else if(this.maxmarks.value === "") {
                      alert("Please Enter maxmarks");
                  this.maxmarks.focus();
                  return false;
              }
               else if(this.year.value === "") {
                      alert("Please Enter Year");
                  this.year.focus();
                  return false;
              }
               
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsDiscriptiveTest.php",
                      data:$('#insertDiscriptiveTestform').serialize(),
                      success: function(data){
                             alert(data.trim());
                             $('#insertDiscriptiveTestform').each(function(){
                              this.reset();
                              
                          });
                          onLoadDiscriptiveTest();
                      }
                      
                  });
              }
              
              return false;
          });
  </script>
    <script>
      /* Select Prelims Exams */  
      function onLoadDiscriptiveTest(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelDiscriptiveTest.php",
                    data:{discriptivetest_id:''},
                    success: function(data) {
//                       alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data7 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            $("#data7").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
                                .append($('<td/>').html(article.papername))
                                .append($('<td/>').html(article.papertype))
                                .append($('<td/>').html(article.duration))
                                .append($('<td/>').html(article.maxmarks))
                                .append($('<td/>').html(article.year))
                                //.append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_discriptivetest_row'}).append("Delete").click(function (){delete_discriptivetest_row(article.discriptivetest_id); })))
                             );
                        });
                        
                    }
                });  
             }
  </script>
   <script>
       /* Delet Descriptive type Test */  
      function delete_discriptivetest_row(discriptivetest_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelDiscriptiveTest.php",
                            data:{"discriptivetest_id":discriptivetest_id},
                            success: function(data){
                            alert(data.trim());
                             onLoadDiscriptiveTest();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
  <script>
       /* Insert  GD*/  
      $(document).ready(function() {
          $("#addGroupDiscussion").click(function(){
            $("#insGroupDiscussion").modal('show');}); 
         });
         $("#insGroupDiscussion").on('shown.bs.modal', function(){
                
          });
                $('#insertGroupDiscussionform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
//                        alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertGroupDiscussionform').find('#selExamName').append(a);                    
                        });
                        $('#insertGroupDiscussionform').find('#selExamName').trigger("change");
                        }
                        });
                  $('#insertGroupDiscussionform').find('#selExamName').change(function()
                  {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertGroupDiscussionform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertGroupDiscussionform').find('#selExamSubCat').append(b);                    
                        });
                        $('#insertGroupDiscussionform').find('#selExamSubCat').trigger("change");
                        }
                        });  
                        return false;
                });
                  $('#insertGroupDiscussionform').find('#selExamSubCat').change(function()
                  {
                 var exam_sub_cat_id=$(this).val();
                 var dataString = 'exam_sub_cat_id='+exam_sub_cat_id;
                 $('#insertGroupDiscussionform').find('#selExamPhase').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsPhases2.php",
                        data:{"exam_sub_cat_id":exam_sub_cat_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_phase_name).attr({'value':article.exam_phase_id});
                        $('#insertGroupDiscussionform').find('#selExamPhase').append(b);                    
                        });
                        }
                        });  
                        return false;
                });
                $('#insertGroupDiscussionform').submit(function() { 
                    if(this.content.value===""){
                        alert("Please Enter Group Discussion Content")
                 }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsGroupDiscussion.php",
                      data:$('#insertGroupDiscussionform').serialize(),
                      success: function(data){
                             alert(data.trim());
                          $('#insertGroupDiscussionform').each(function(){
                              this.reset();
                              
                          });
                      onLoadGroupDiscussion();
                      }
                      
                  });
              }
              
              return false;
          });
            
  </script>
   <script>
      /* Select Exams Sections */  
      function onLoadGroupDiscussion(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelGroupDiscussion.php",
                    data:{groupdiscussion_id:''},
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data8 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            groupdiscussion[article.id]={};
                            groupdiscussion[article.id]["groupdiscussion_id"]=article.groupdiscussion_id;
                            groupdiscussion[article.id]["exam_name"]=article.exam_name;
                            groupdiscussion[article.id]["exam_sub_cat_name"]=article.exam_sub_cat_name;
                            groupdiscussion[article.id]["exam_phase_name"]=article.exam_phase_name;
                            groupdiscussion[article.id]["content"]=article.content;
                            groupdiscussion[article.id]["year"]=article.year;
                            $("#data8").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
                                .append($('<td/>').css({'text-align':'justify'}).html(article.content))
                                .append($('<td/>').html(article.year))
//                              .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_GroupDiscussion_row'}).append("Delete").click(function (){delete_GroupDiscussion_row(article.groupdiscussion_id); })))
                             );
                        });
                    }
                });   
             }
  </script> 
    <script>
       /* Delete Group Discussion Test */  
      function delete_GroupDiscussion_row(groupdiscussion_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelGroupDiscussion.php",
                            data:{"groupdiscussion_id":groupdiscussion_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadGroupDiscussion();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
  
  
   <script>
       /* Insert  PI*/  
      $(document).ready(function() {
          $("#addPersonalInterview").click(function(){
            $("#insPersonalInterview").modal('show');}); 
         });
         $("#insPersonalInterview").on('shown.bs.modal', function(){
                
          });
                $('#insertPersonalInterviewform').find('#selExamName').empty();
                        $.ajax({
                        type:"post",
                        url:"../server/controller/SelExams.php",
                        success: function(data) { 
//                        alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var a=$("<option></option>").append(article.exam_name).attr({'value':article.exam_id,'id':article.exam_name});
                        $('#insertPersonalInterviewform').find('#selExamName').append(a);                    
                        });
                        $('#insertPersonalInterviewform').find('#selExamName').trigger("change");
                        }
                        });
                  $('#insertPersonalInterviewform').find('#selExamName').change(function()
                  {
                 var exam_id=$(this).val();
                 var dataString = 'exam_id='+ exam_id;
                 $('#insertPersonalInterviewform').find('#selExamSubCat').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsSubCategory2.php",
                        data:{"exam_id":exam_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_sub_cat_name).attr({'value':article.exam_sub_cat_id});
                        $('#insertPersonalInterviewform').find('#selExamSubCat').append(b);                    
                        });
                        $('#insertPersonalInterviewform').find('#selExamSubCat').trigger("change");
                        }
                        });  
                        return false;
                });
                  $('#insertPersonalInterviewform').find('#selExamSubCat').change(function()
                  {
                 var exam_sub_cat_id=$(this).val();
                 var dataString = 'exam_sub_cat_id='+exam_sub_cat_id;
                 $('#insertPersonalInterviewform').find('#selExamPhase').empty();
//                 alert(dataString);
                   $.ajax({
                        type:"post",
                        url:"../server/controller/SelExamsPhases2.php",
                        data:{"exam_sub_cat_id":exam_sub_cat_id},
                        success: function(data) { 
//                            alert(data.trim());
                        var duce = jQuery.parseJSON(data); //here data is passed to js object
                        $.each(duce, function (index, article) {
                        var b=$("<option></option>").append(article.exam_phase_name).attr({'value':article.exam_phase_id});
                        $('#insertPersonalInterviewform').find('#selExamPhase').append(b);                    
                        });
                        }
                        });  
                        return false;
                });
                $('#insertPersonalInterviewform').submit(function() { 
                    if(this.content.value===""){
                        alert("Please Enter Group Discussion Content")
                 }
               else {      
                  $.ajax({
                      type:"post",
                      url:"../server/controller/InsPersonalInterview.php",
                      data:$('#insertPersonalInterviewform').serialize(),
                      success: function(data){
                          $('#insertPersonalInterviewform').each(function(){
                              this.reset();
                              
                          });
                      onLoadPersonalInterview();
                      }
                      
                  });
              }
              
              return false;
          });
            
  </script>
  <script>
      /* Select Personal Interview */  
      function onLoadPersonalInterview(){
                    $.ajax({
                    type:"post",
                    url:"../server/controller/SelPersonalInterview.php",
                    data:{personalinterview_id:''},
                    success: function(data) {
//                        alert(data.trim());
                       var duce = jQuery.parseJSON(data);
                        $("#data9 tr:has(td)").remove();
                        $.each(duce, function (index, article){
                            
                            $("#data9").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.exam_name))
                                .append($('<td/>').html(article.exam_sub_cat_name))
                                .append($('<td/>').html(article.exam_phase_name))
                                .append($('<td/>').css({'text-align':'justify'}).html(article.content))
                                .append($('<td/>').html(article.year))
//                              .append($('<td/>').html($("<button>").addClass('btn btn-success btn-xs').attr({'type':'button','id':'edit_exam_sub_cat_row'}).append("Edit").click(function (){edit_exam_sub_cat_row(article.exam_id,article.exam_name); })))
                                .append($('<td/>').html($("<button>").addClass('btn btn-danger btn-xs').attr({'type':'button','id':'delete_PersonalInterview_row'}).append("Delete").click(function (){delete_PersonalInterview_row(article.personalinterview_id); })))
                             );
                        });
                    }
                });   
             }
  </script> 
  
    <script>
       /* Delete PI */  
      function delete_PersonalInterview_row(personalinterview_id){
           $("#delExams").modal('show'); 
                 $('#delete2').off();
                 $('#delete2').click(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelPersonalInterview.php",
                            data:{"personalinterview_id":personalinterview_id},
                            success: function(data){
                            alert(data.trim());
                            onLoadPersonalInterview();
                            $("#delExams").modal('hide');; 
                      } 
                      
                      });
                    
                    return false;
    });
      }
 
  
  </script>
</html>


