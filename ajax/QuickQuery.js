$(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#quick-query').submit(function() { 
        if(this.name.value === "") {
//             alert("Hello");
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                  $("#errormsg").find("#msg").empty(); 
                  $("#errormsg").find("#msg").append("Please choose valid 'Name'.") ;
                 
            });
            this.name.focus();
            return false;
        }
        else if(this.email.value === "" || !emailfilter.test(this.email.value)) {
//                         alert("Hello");
             $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                  $("#errormsg").find("#msg").empty(); 
                  $("#errormsg").find("#msg").append("Please choose valid 'Email'.") ;
                 
            });
            this.email.focus();
            return false;
        }
        else if(this.contact.value == ""|| !contactnofilter.test(this.contact.value)) {
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                $("#errormsg").find("#msg").empty();  
                $("#errormsg").find("#msg").append("Please enter valid 'Contact No'.") ;
                        
            });
           
            this.contact.focus();
            return false;
        }
        else if (this.subject.value == "") {
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                $("#errormsg").find("#msg").empty();  
                $("#errormsg").find("#msg").append("Please enter valid 'Subject'.") ;
                
            });
            this.subject.focus();
            return false;
        }
        
        else if (this.message.value == "") {
            $('#warningmsg').modal('show'); 
            $("#warningmsg").on('shown.bs.modal', function(){
                $("#errormsg").find("#msg").empty();  
                $("#errormsg").find("#msg").append("Please enter valid 'Message'.") ;
                
            });
            this.message.focus();
            return false;
        }
        else {      
           
            $.ajax({
                type:"post",
                url:"server/controller/InsQuickQuery.php",
                data:$('#quick-query').serialize(),
                success: function(data){
//                   alert(data.trim());
                    $('#enquirysuccess').modal('show'); 
                    $("#enquirysuccess").on('shown.bs.modal', function(){
                        //                        $('#msg').append(data) ;
                    }); 
                    setTimeout(function(){
                        $('#enquirysuccess').modal('hide')
                    }, 10000);
                    $('#quick-query').each(function(){
                        this.reset();
                    });
                  $('#quick-query').find('#submit').attr("disabled", false); 
                }
              
            });
        }
        closeNav();
        return false;
    });
});

