$(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#UserRegistration').submit(function() { 
        if(this.user_name.value === "") {
                  $("#mySignUp").find("#errorMsg").empty(); 
                  $("#mySignUp").find("#errorMsg").append("Please choose valid 'Name'.") ;
                  this.user_name.focus();
                  return false;
        }
        else if(this.email.value === "" || !emailfilter.test(this.email.value)) {
            $("#mySignUp").find("#errorMsg").empty(); 
            $("#mySignUp").find("#errorMsg").append("Please choose valid 'Email'.") ;
            this.email.focus();
            return false;
        }
        else if(this.contact_no.value == ""|| !contactnofilter.test(this.contact_no.value)) {
            $("#mySignUp").find("#errorMsg").empty(); 
            $("#mySignUp").find("#errorMsg").append("Please choose valid 'Contact Number'.") ;
            this.contact_no.focus();
            return false;
        }
        else if (this.password.value == "") {
            $("#mySignUp").find("#errorMsg").empty(); 
            $("#mySignUp").find("#errorMsg").append("Please choose valid 'Password'.") ;
            this.password.focus();
            return false;
        }
        
//        else if (this.terms.value == "") {
//            $("#mySignUp").find("#errorMsg").empty(); 
//            $("#mySignUp").find("#errorMsg").append("Please accept terms and conditions") ;
//            this.terms.focus();
//            return false;
//        }
        else { 
             $("#mySignUp").find("#errorMsg").empty(); 
            $.ajax({
                type:"post",
                url:"../server/controller/InsUserDetails.php",
                data:$('#UserRegistration').serialize(),
                success: function(data){
//                    alert(data.trim());
                   $("#mySignUp").find("#successMsg").empty(); 
                    $("#mySignUp").find("#successMsg").append("Thanks for being our awesome customer!") ;
                    
                    $('#UserRegistration').each(function(){
                        this.reset();
                        setTimeout(function(){
                       $("#mySignUp").find("#successMsg").empty();    
                       $("#mySignUp").modal("hide");
                       
                    }, 10000);
                    
                    });
                 }
               
            });
        }
       return false;
    });
});

