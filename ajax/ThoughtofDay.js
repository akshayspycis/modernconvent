$(document).ready(function() {
    
    $('#thoughtofdayform').off("submit");
    $('#thoughtofdayform').submit(function() {
        
        if(this.author.value === "") {
            alert("Please Fill Author Name");
            this.author.focus();
            return false;
        }
        else if(this.content_tmp.value===""){
            alert("Please Fill Content");
            this.content_tmp.focus();
            return false;
        }else {
                this.content.value = this.content_tmp.value.replace("'",'_');
                $.ajax({
                    type:"post",
                    url:"server/controller/InsThoughtofDay.php",
                    data:$("#thoughtofdayform").serialize(),
                    success: function(data){
                        alert(data.trim());
                       $('#thoughtofdayform').each(function(){
                              this.reset();
                              
                          });
                        $('#success_msg_demo').modal('show'); 
                        setTimeout(function (){
                         $('#success_msg_demo').modal('hide'); 
                        },5000);

                    }
                });
        }
        return false;
    });
    
        
});

