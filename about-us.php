<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>About Us</title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header "  onload="loadHTML('about-us')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
                            
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <div class="container">
                                    <ol class="breadcrumb">
                                        <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.php">Home</a></li>
                                        <li class="active">About Us</li>
                                
                                    </ol>
                                </div>
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
			<div id="page-start"></div>
                        <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
<!--                                    <p id="para-font">&nbsp;
                                        <b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>
                                     </p>-->
                                </div>
                            <div class="col-md-10">

                                   <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                                       <a href = "assets/pdf/admission-form.pdf" target="_blank" style="color:black;font-weight:bold;">Admissions open for the academic year 2017-18 in Modern Group of Schools.<span style="color:yellow">Click here to Download Form </span>For details contact<span style="color:#f1fa00;font-weight:bold" >
                                               <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '7566931851, 9713003663';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '7049923643, 9926740041'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '9926740041'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                              
                                           
                                           </span> . Hurry! Only few seats left.</a>
                                  </marquee>
                            </div>
                            </div>
                             
                        </div>
			<!-- section start -->
			<!-- ================ -->
                           <div class="clearfix"></div>
			<!-- section end -->
                        <!-- section start -->
			<!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-8">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;">
                                                         <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '<strong>Modern Convent School, Mandideep</strong>';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '<strong>Modern Convent School, Obedullaganj</strong>'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '<strong>Modern English Medium Higher Secondary School, Tamot</strong>'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                                    </h1>
                                                    <div class="separator"></div>
                                                         <p id="cpara">On behalf of our organisation ,I am pleased to present our website Moderngroupofschools.com Our Organisation has completed its 14 years of existence.Our organisation administrative arrangements are now inplace & functioning. From the very beginning we believe that development has to be holistic if we wish to break the vicious cycle of poverty and to bring real fruits to the must needy, public health, education, women empowerment and community development continue to be the prime areas of our work. We wish to bring a substantial change on the ground by focusing our energies in these sectors as well as building our understanding about related issues. We firmly believe that a conductive and productive environment can’t be achieved unless all these parameters of the development equation are suitably answered.</p>
                                                         <p id="cpara">As an Educational Organisation we are working since from 2002 and day by day we are enhancing our ability, work area and of course the way of serving people. Presently we have 06 authorized VTPs (approved by NCVT), 118 STAR & PMKVY Centers in all over India and 03 Schools. After successfully working for more than 14 years we have our networks in other states also. We have a great team of experts in every possible educational sector many of us are from teaching background. In short with years of experience we can confidently work in any sector related with training and providing level education. We do have tie-ups with industries for the placement of students.</p>
                                                         <p id="cpara">Till date we had provided vocational training to approx 22000 students. We also worked for NSDC project “STAR”(STANDARD TRAINING AND REWARD SCHEME) with affilation from “RASCI”(RETAILERS ASSOCIATION’S SKILL COUNCIL OF INDIA) a , “TSSC”(TELECOM SECTOR SKILL COUNCIL OF INDIA).We are also Working for Pradhan Mantri Koushal Vikas Yojna with affiliation from “IISSSC”(Indian Iron & Steel Sector Skill Council) , “ESSCI”(Electronics Sector Skill Council of India), “BWSSC”(Beauty & Wellness Sector Skill Council), “LSSSC”(Life Science Sector Skill Council), “PSSCI”(Power Sector Skill Council).</p>
                                                         <p>&nbsp;</p>
                                                        
						</div>
						<div class="col-md-4" style="background:url('assets/images/bg/bigbg2.PNG') no-repeat;background-position:0px 70px;">
                                                   <p>&nbsp;</p>
                                                    <div class="overlay-container overlay-visible" style="height:180px;box-shadow:0 6px 10px rgba(0,0,0, 0.25);">
                                                       <?php  if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];
                                                             switch ($scat) {
                                                        case "1": ?>
                                                        <img src="assets/images/modern-mandideep.jpg"   />
                                                        <a href="assets/images/modern-mandideep.jpg"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                        <?php   break;
                                                        case "2": ?>
                                                        <img src="assets/images/modern-obj.jpg"   />
                                                         <a href="assets/images/modern-obj.jpg"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                        <?php    break;
                                                        case "3": ?>
                                                         <img src="assets/images/modern-tamot1.jpg"   />
                                                         <a href="assets/images/modern-tamot.JPG"   class="popup-img overlay-link" ><i class="icon-plus-1"></i></a>
                                                         <?php    break;
                                                       
                                                         }
                                                      
                                                        }?>
                                                    </div>
                                                   <p>&nbsp;</p>
                                               </div>
						
					</div>
                                    <p>&nbsp;</p>
                                 </div>
                        </section>
			<section class="full-width-section">
					<div class="  border-bottom-clear">
                                            <img class="to-right-block" src="assets/images/SVS_7308.JPG" alt="">
						
					</div>
					<div class="full-text-container default-bg border-bottom-clear" style="background:#1da913 url('assets/images/bg/overlay.png')">
						<h2 id="heading-font">Our Mission</h2>
						<div class="separator-2 visible-lg"></div>
                                                <p id="cpara" style="font-size:22px;">Our mission is to synergies the academics with industry relevance to provide conductive environment for research, training, consultation and quality education, which will enable students to excel in the field of Excellence.</p>
						<div class="separator-3 visible-lg"></div>
						
					</div>
				</section>
                     <section class="full-width-section">
					<div class="full-text-container bg-primary border-bottom-clear" style="background:#0d5995 url('assets/images/bg/overlay.png')">
                                            <h2 id="heading-font" style="color:white">Our Vision</h2>
						<div class="separator-2 visible-lg"></div>
                                                <p id="cpara" style="font-size:22px;">To work towards creating a better India which provides basic health, education and empowerment to every citizen and aims to be a quality oriented organization, dedicated to creation, application, dissemination of knowledge, spirit of competitive excellence, building up a generation of global citizens & value driven professionals for the corporate world.</p>
						<div class="separator-3 visible-lg"></div>
						
					</div>
                                      <div class="border-bottom-clear">
                                          <img class="to-right-block" src="assets/images/SVS_7202.JPG" alt="">
						<div class="full-image-overlay text-right">
							
						</div>
					</div>
					
				</section>
			<section class="full-width-section">
                            <div class="border-bottom-clear" >
                                <img class="to-right-block" src="assets/images/SVS_6995.JPG" alt="">
						
					</div>
					<div class="full-text-container default-bg border-bottom-clear" style="background:#d78600 url('assets/images/bg/overlay.png')">
						<h2 id="heading-font">Our Objective</h2>
						<div class="separator-2 visible-lg"></div>
                                                <p id="cpara" style="font-size:22px;">Strengthening the student talent to become a successful entrepreneur, which will have way for employment opportunities and sustainability in all spheres of life.</p>
						<div class="separator-3 visible-lg"></div>
					</div>
				</section>
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
	</body>
</html>
