<?php include 'includes/session.php'; ?>    
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
        <head>
            <meta charset="utf-8">
            <title>Contact Us  |  Modern Group of Schools</title>
           
            <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans front-page transparent-header" onload="loadHTML('contact')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			<?php include 'includes/header.php' ;  ?>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('assets/images/bg/22.jpg'); background-position: 50% 30%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container">
					<div class="container">
						<ol class="breadcrumb">
                                                    <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.php">Home</a></li>
							<li class="active">Contact us</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					<div class="row">
                                            <div class="col-md-12 text-center  pv-20">
                                                <h1 class="page-title text-center">Contact Us</h1>
                                                <div class="separator"></div>
                                                <p class="lead text-center" style="font-weight:bold">It would be great to hear from you! Just drop us a line and ask for anything with which you think we could be helpful. We are looking forward to hearing from you!</p>
                                                <ul class="list-inline mb-20 text-center" style="font-weight:bold">
                                                     <?php
                                                                                    if(isset($_SESSION['scat'])){
                                                                                    $scat = $_SESSION['scat'];
                                                                                    switch ($scat) {
                                                                                    case "1": ?>
                                                                                    <li><a href="#" class="link-dark"><i class="text-default fa fa-phone pl-10 pr-5"></i>7566931851, 9713003663</a></li>
                                                                                    
                                                                                    <?php   break;
                                                                                    case "2": ?>
                                                                                    <li><a href="#" class="link-dark"><i class="text-default fa fa-phone pl-10 pr-5"></i>9926740041, 7049923643</a></li>
                                                                                   <?php    break;
                                                                                    case "3": ?>
                                                                                     <li><a href="#" class="link-dark"><i class="text-default fa fa-phone pl-10 pr-5"></i>9926740041</a></li>
                                                                                    
                                                                                    <?php    break;


                                                                                    }

                                                                                    }
                                                                                    ?> 
                                                    
<!--                                                    <li><a href="" class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-5"></i>contactus@vibrantcareer.com</a></li>-->
                                                </ul>
                                                <div class="separator"></div>
<!--                                                <ul class="social-links circle animated-effect-1 margin-clear text-center space-bottom">
                                                    <li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                    <li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                                    <li class="linkedin"><a target="_blank" href="http://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
                                                    <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                                                    <li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
                                                                                    
                                                </ul>-->
                                            </div>
					</div>
				</div>
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>
                            <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
<!--                                    <p id="para-font">&nbsp;
                                        <b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>
                                     </p>-->
                                </div>
                            <div class="col-md-10">

                                   <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                                       <a href = "assets/pdf/admission-form.pdf" target="_blank" style="color:black;font-weight:bold;">Admissions open for the academic year 2017-18 in Modern Group of Schools.<span style="color:yellow">Click here to Download Form </span>For details contact<span style="color:#f1fa00;font-weight:bold" >
                                               <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '7566931851, 9713003663';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '7049923643, 9926740041'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '9926740041'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                              
                                           
                                           </span> . Hurry! Only few seats left.</a>
                                  </marquee>
                            </div>
                            </div>
                             
                        </div>
			<!-- section start -->
			<!-- ================ -->
                        <section class="light-gray-bg pv-30 clearfix"  style="background:#f8f8f8 ;box-shadow:inset 0 2px 7px rgba(0,0,0, 0.25);">
                            <div class="container">
                               <div class="main col-md-12 space-bottom">
							<h2 class="title">Feel free to contact us</h2>
							<div class="row">
								<div class="col-md-6">
									
                                                                    <form role="form" class="" method="post" id="contact-query">
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    
                                                                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact">
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    
                                                                                    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                                                                                </div>
            
                                                                                <div class="form-group">
                                                                                    
                                                                                    <textarea class="form-control" rows="3" id="message" name="message" placeholder="Message"></textarea>
                                                                                </div>
                                                                                <input type="submit" name="submit" id="submit" class="btn btn-default" value="Send"/>
                                                                            </form>
									
								</div>
								<div class="col-md-6">
                                                                    <?php

                                                                    if(isset($_SESSION['scat'])){
                                                                    $scat = $_SESSION['scat'];

                                                                    switch ($scat) {
                                                                    case "1": ?>
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4364.398917758895!2d77.51381494153914!3d23.09682477961176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x397c47cab1454ba1%3A0x926d7d8245c1c17b!2sModern+Convent+School!5e0!3m2!1sen!2s!4v1492077651889" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                                    <?php   break;
                                                                    case "2": ?>
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d1836.299104953702!2d77.58305129736729!3d23.00179782474998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x397c366cb6e12c87%3A0xbefe1c63b0e4716d!2smodern+convent+school+obedullaganj!3m2!1d23.0026181!2d77.585382!5e0!3m2!1sen!2sin!4v1492598167698" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                                    <?php    break;
                                                                    case "3": ?>
                                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d58759.789393811334!2d77.60268110620956!3d23.005890449984953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x397c36bb97a61eb7%3A0x71d9a45b270ea892!2sModern+English+Medium+Higher+Secondary+School%2C+Tamot!3m2!1d23.005894299999998!2d77.63770079999999!5e0!3m2!1sen!2sin!4v1492599641491" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                                    <?php    break;


                                                                    }
                                                                    }   ?>
									
								</div>
							</div>
						</div>   
                                
                            </div>
                        </section>
			   
			<!-- section end -->

			<!-- section -->
			<!-- ================ -->
		
			<!-- section end -->

			<!-- section start -->
			<!-- ================ -->
                        
			                   
			<!-- section end -->
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include 'includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		 <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/ContactQuery.js"></script>
	</body>

</html>

