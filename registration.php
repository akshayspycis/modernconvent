<?php include 'includes/session.php'; ?>    
   
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Admission Form</title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
            <style>
                .form-group label{
                    font-weight:bold;
                    font-size:16px;
                }
                .form-group select option, .form-group input, .form-group textarea{
                    font-weight:bold;
                   
                }
            </style>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('registration')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <div class="container">
                                    <ol class="breadcrumb">
                                        <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
                                        <li class="active">Admission Form</li>
                                   </ol>
                                </div>
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
     <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
<!--                                    <p id="para-font">&nbsp;
                                        <b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>
                                     </p>-->
                                </div>
                            <div class="col-md-10">

                                   <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                                       <a href = "assets/pdf/admission-form.pdf" target="_blank" style="color:black;font-weight:bold;">Admissions open for the academic year 2017-18 in Modern Group of Schools.<span style="color:yellow">Click here to Download Form </span>For details contact<span style="color:#f1fa00;font-weight:bold" >
                                               <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '7566931851, 9713003663';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '7049923643, 9926740041'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '9926740041'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                              
                                           
                                           </span> . Hurry! Only few seats left.</a>
                                  </marquee>
                               </div>
                            </div>
                             
                        </div>
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					
                                <div class="row">
                                    <div class="" style="width:80%;margin-left:auto;margin-right:auto;background:rgb(248,248,248);height:auto;box-shadow: 0 4px 10px rgba(0,0,0, 0.50)">
                                        <div  style="background:url('assets/images/bg/nursery-schools-for-kids1.jpg');background-size:cover;height:180px;">
                                            <div style="position:relative;z-index:999;width:100%;background:rgba(0,0,0,0.4);height:180px;padding-top:30px;">
                                                 <h1 class="text-center " id="heading-font" style="text-transform:none;color:white;"><strong>Admission Form</strong> </h1>
                                            </div>
					</div>
                                       <div class="separator"></div>
 <form id ="insertform" name="registration" action="registration.php" method="post" enctype="multipart/form-data" >    
        <div class="modal-body">
        <div class="row">
               <div class="col-xs-12 col-sm-12">
                <div class="box">
               <div class="box-body">
                        <div class="col-sm-12">
                        <h3 class="text-center " id="heading-font" style="text-transform:none;"><strong>Student Details</strong> </h3>
                        <div class="form-group">
                            <label for="student name">Admission In Class </label>
                            <select class="form-control" name="class_name" id="class_name">
                            <option value="Pre School">Pre School</option>
                            <option value="1st">1st</option>
                            <option value="2nd">2nd</option>
                            <option value="3rd">3rd</option>
                            <option value="4th">4th</option>
                            <option value="5th">5th</option>
                            <option value="6th">6th</option>
                            <option value="7th">7th</option>
                            <option value="8th">8th</option>
                            <option value="9th">9th</option>
                            <option value="10th">10th</option>
                            <option value="11th">11th</option>
                            <option value="11th">12th</option>
                            </select>
                        </div>
                  <!-- /.form-group -->
                 
                        <div class="form-group">
                        <label for="Fname">First Name </label>
                        <input type="text" class="form-control"  name="first_name" id="first_name">
                        </div>
                        <div class="form-group">
                        <label for="Mname">Middle Name </label>
                        <input type="text" class="form-control"  name="middle_name">
                        </div>
                        <div class="form-group">
                        <label for="Lname">Last Name </label>
                        <input type="text" class="form-control"  name="last_name">
                        </div>
                        <div class="form-group">
                        <label for="date">Date of Birth</label>
                        <input type="text" class="form-control"  placeholder="DD/MM/YYYY" name="dob" id="dob">
                        </div>
                        <div class="form-group">
                            <label for="sel1">Gender &nbsp;&nbsp;&nbsp;</label>
                            <input type="radio" name="gender" id="gender" value="male" > Male &nbsp;&nbsp;&nbsp;
                            <input type="radio" name="gender" id="gender" value="female"> Female
                        </div>
                          <div class="form-group">
                         <label for="sel1">Category</label>
                        <select class="form-control" name="category" id="category">
                          <option value="sc">SC</option>
                          <option value="st">ST</option>
                          <option value="obc">OBC</option>
                          <option value="gen">GEN</option>
                        </select>
                        </div>
                        <div class="form-group">
                        <label for="Cast">Caste</label>
                        <input type="text" class="form-control"   name="cast" id="cast">
                        </div>
                         <div class="form-group">
                        <label for="nationality">Nationality</label>
                        <input type="text" class="form-control"   name="nationality" id="nationality" value="Indian">
                        </div>
                         <div class="form-group">
                        <label for="ssid">Student Samagra Id No.</label>
                        <input type="text" class="form-control"   name="student_samgraid" id="student_samgraid" >
                        </div>
                        <div class="form-group">
                        <label for="aid"> Aadhaar No. </label>
                        <input type="text" class="form-control"   name="aadhar_no" id="aadhar_no" >
                        </div>
                        <div class="form-group">
                        <label for="fsid">Family Samagra Id No. </label>
                        <input type="text" class="form-control"   name="family_samgraid" id="family_samgraid" >
                        </div>
                        <div class="form-group">
                        <label for="contact">Contact Number</label>
                        <input type="number" class="form-control"   name="contact" id="contact" >
                        </div>
                        <div class="form-group">
                        <label for="contact">Email ID</label>
                        <input type="email" class="form-control"   name="email" id="email" >
                        </div>
                        <div class="form-group">
                         <label for="sel1">Select School Name</label>
                          <select class="form-control" name="school_name" id="school_name">
                          <option value="Modern Convent School Mandideep">Modern Convent School Mandideep</option>
                          <option value="Modern Convent School Obedullaganj">Modern Convent School Obedullaganj</option>
                          <option value="Modern English Medium Higher Secondary School Tamot">Modern English Medium Higher Secondary School Tamot</option>
                        </select>
                        </div>
                  
               
                 <div class="form-group">
                    <label>Residential Address</label>
                    <input type="text" class="form-control"   name="address" id="address" >
                  </div><!-- /.form-group -->
                  <div class="form-group">
                    <label>Locality</label>
                    <input type="text" class="form-control"   name="locality" id="locality" >
                  </div>
                  <div class="form-group">
                    <label>Village </label>
                    <input type="text" class="form-control"   name="village" id="village" >
                  </div>
                  <div class="form-group">
                    <label>Gram Panchayat </label>
                    <input type="text" class="form-control"   name="grampanchayat" id="grampanchayat" >
                  </div>
                  <div class="form-group">
                    <label>Distt. Panchayat /Nagar Panchayat /Nagar Nigam /Nagar Palika/Tahsil </label>
                    <input type="text" class="form-control"   name="nagarpanchayat" id="nagarpanchayat" >
                  </div>
                  <div class="form-group">
                    <label>District</label>
                    <input type="text" class="form-control"   name="district" id="district" >
                  </div>
                  <div class="form-group">
                    <label>Pincode</label>
                    <input type="text" class="form-control"   name="pincode" id="pincode" >
                  </div>
                  <div class="form-group">
                    <label>State</label>
                    <input type="text" class="form-control"   name="state" id="state" >
                  </div>
                     <div class="form-group">
                        <label for="image">Add Image</label>
                         <input type="file"  name="file" id="file">
                      
                     </div>
                      <h3 class="text-center " id="heading-font" style="text-transform:none;"><strong>Parents Details</strong> </h3>
                        <div class="form-group">
                        <label for="">Mother's Name</label>
                        <input type="text" class="form-control"  name="mothers_name" id="mothers_name">
                        </div>
                        <div class="form-group">
                        <label for="">Mother's Qualification  </label>
                        <input type="text" class="form-control"  name="mothers_qualifications" id="mothers_qualifications">
                        </div>
                        <div class="form-group">
                        <label for="">Mother's Occupation</label>
                        <input type="text" class="form-control"  name="mothers_occupation" id="mothers_occupation">
                        </div>
                        <div class="form-group">
                        <label for="">Mother's Contact</label>
                        <input type="text" class="form-control"  name="mothers_contact" id="mothers_occupation">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Father's Name</label>
                        <input type="text" class="form-control"  name="fathers_name" id="fathers_name">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Father's Qualifications</label>
                        <input type="text" class="form-control"  name="fathers_qualifications" id="fathers_qualifications">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Father's Occupation</label>
                        <input type="text" class="form-control"  name="fathers_occupation" id="fathers_occupation">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Father's Contact</label>
                        <input type="text" class="form-control"  name="fathers_contact" id="fathers_contact">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Father's email id</label>
                        <input type="text" class="form-control"  name="fathers_emailid" id="fathers_emailid">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Name of Local Guardian </label>
                        <input type="text" class="form-control"  name="local_guardian" id="local_guardian">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Local Guardian Mobile No. </label>
                        <input type="text" class="form-control"  name="local_guardian_contact" id="local_guardian_contact">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Local Guardian Occupation</label>
                        <input type="text" class="form-control"  name="local_guardian_occupation" id="local_guardian_occupation">
                        </div>
                        <div class="form-group">
                        <label for="Fname">Last School Attended</label>
                        <input type="text" class="form-control"  name="last_school_attended" id="last_school_attended">
                        </div>
                      <div class="form-group">
                        <label for="Fname">Last School Scholar No.</label>
                        <input type="text" class="form-control"  name="last_school_scholar" id="last_school_scholar">
                        </div>
                      <div class="form-group">
                          <label for="Fname">Last Class</label>
                          <input type="text" class="form-control"  name="last_class" id="last_class">
                      </div>
                      <div class="form-group">
                          <label for="Fname">Passed Year</label>
                          <input type="text" class="form-control"  name="passed_year" id="passed_year">
                      </div>
                      <div class="form-group">
                          <label for="Fname">Name of Board Passed</label>
                          <input type="text" class="form-control"  name="passed_board" id="passed_board">
                      </div>
                      <div class="form-group">
                          <label for="Fname">Declaration</label><br/>
                          <input type="checkbox" value="ok" name="declaration" id="declaration" checked="checked"> I have gone through all the important instructions/ rules and discipline of the institutions.
 Above information’s to the best of my knowledge are correct. I shall abide myself all the
 Rules and Regulations
                      </div>
                      
      <!-- /.col -->
              </div><!-- /.row -->
                  </div><!-- /.box-body -->

            
              </div><!-- /.box -->
            </div><!-- /.col -->
            
       
        </div><!-- /.box -->


        <div class="modal-footer">
           
        <button type="submit" name="submit" class="btn btn-default">Submit</button>
        <button type="reset" class="btn btn-danger">Cancel</button>
        
        </div>
        </div>
       </form>
                                <?php
                                include 'includes/db.php'; 
                                if(isset($_POST['submit'])){?>
                                    
                                       
                                       <?php
                                             // image upload code begin
$file_exts = array("jpg", "bmp", "jpeg", "gif", "png");
$upload_exts = end(((explode(".", $_FILES["file"]["name"]))));
if((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/jpg")
|| ($_FILES["file"]["type"] == "image/png")
|| ($_FILES["file"]["type"] == "image/pjpeg"))
&& ($_FILES["file"]["size"] < 2000000)
&& in_array($upload_exts, $file_exts))
{
if ($_FILES["file"]["error"] > 0)
{
echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
}
else
{
//echo "Upload: " . $_FILES["file"]["name"] . "<br>";
//echo "Type: " . $_FILES["file"]["type"] . "<br>";
//echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
//echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
// Enter your path to upload file here
if (file_exists("assets/images/registrationpics/" .$_FILES["file"]["name"]))
{
echo "<div class='error'>"."(".$_FILES["file"]["name"].")".
" already exists. "."</div>";
}
else
{


$image = "assets/images/registrationpics/" . $_FILES["file"]["name"];
                                            $class_name= $_POST['class_name'];
                                            $first_name= $_POST['first_name']; 
                                            $middle_name= $_POST['middle_name'];
                                            $last_name= $_POST['last_name'];
                                            $dob= $_POST['dob'];
                                            $gender= $_POST['gender'];
                                            $category= $_POST['category'];
                                            $cast= $_POST['cast'];
                                            $nationality= $_POST['nationality'];
                                            $student_samgraid= $_POST['student_samgraid'];
                                            $aadhar_no= $_POST['aadhar_no'];
                                            $family_samgraid= $_POST['family_samgraid'];
                                            $contact= $_POST['contact'];
                                            $email= $_POST['email'];
                                            $school_name= $_POST['school_name'];
                                            $address= $_POST['address'];
                                            $locality= $_POST['locality'];
                                            $village= $_POST['village'];
                                            $grampanchayat= $_POST['grampanchayat'];
                                            $nagarpanchayat= $_POST['nagarpanchayat'];
                                            $district= $_POST['district'];
                                            $pincode= $_POST['pincode'];
                                            $state= $_POST['state'];
//                                            $image= $_POST['image'];
                                            $mothers_name= $_POST['mothers_name'];
                                            $mothers_qualifications= $_POST['mothers_qualifications'];
                                            $mothers_occupation= $_POST['mothers_occupation'];
                                            $mothers_contact= $_POST['mothers_contact'];
                                            $fathers_name= $_POST['fathers_name'];
                                            $fathers_qualifications= $_POST['fathers_qualifications'];
                                            $fathers_occupation= $_POST['fathers_occupation'];
                                            $fathers_contact= $_POST['fathers_contact'];
                                            $fathers_emailid= $_POST['fathers_emailid'];
                                            $local_guardian= $_POST['local_guardian'];
                                            $local_guardian_contact= $_POST['local_guardian_contact'];
                                            $local_guardian_occupation = $_POST['local_guardian_occupation'];
                                            $last_school_attended= $_POST['last_school_attended'];
                                            $last_school_scholar= $_POST['last_school_scholar'];
                                            $last_class= $_POST['last_class'];
                                            $passed_year= $_POST['passed_year'];
                                            $passed_board= $_POST['passed_board'];
                                            $declaration = $_POST['declaration'];
                                            
                                    $sql = "INSERT INTO admissions(class_name, 
                                                                   first_name, 
                                                                   middle_name,
                                                                   last_name,
                                                                   dob,
                                                                   gender,
                                                                   category,
                                                                   cast,
                                                                   nationality,
                                                                   student_samgraid,
                                                                   aadhar_no,
                                                                   family_samgraid,
                                                                   contact,
                                                                   email,
                                                                   school_name,
                                                                   address,
                                                                   locality,
                                                                   village,
                                                                   grampanchayat,
                                                                   nagarpanchayat,
                                                                   district,
                                                                   pincode,
                                                                   state,
                                                                   image,
                                                                   mothers_name,
                                                                   mothers_qualifications,
                                                                   mothers_occupation,
                                                                   mothers_contact,
                                                                   fathers_name,
                                                                   fathers_qualifications,
                                                                   fathers_occupation,
                                                                   fathers_contact,
                                                                   fathers_emailid,
                                                                   local_guardian,
                                                                   local_guardian_contact,
                                                                   local_guardian_occupation,
                                                                   last_school_attended,
                                                                   last_school_scholar,
                                                                   last_class,
                                                                   passed_year,
                                                                   passed_board,
                                                                   declaration,
                                                                   	datetime
                                                                    )VALUES ('$class_name', "
                                            . "'$first_name', "
                                            . "'$middle_name', "
                                            . "'$last_name', "
                                            . "'$dob', "
                                            . "'$gender', "
                                            . "'$category', "
                                            . "'$cast', "
                                            . "'$nationality', "
                                            . "'$student_samgraid', "
                                            . "'$aadhar_no', "
                                            . "'$family_samgraid', "
                                            . "'$contact', "
                                            . "'$email', "
                                            . "'$school_name', "
                                            . "'$address',"
                                            . "'$locality',"
                                            . "'$village', "
                                            . "'$grampanchayat', "
                                            . "'$nagarpanchayat', "
                                            . "'$district', "
                                            . "'$pincode', "
                                            . "'$state', "
                                            . "'$image', "
                                            . "'$mothers_name', "
                                            . "'$mothers_qualifications', "
                                            . "'$mothers_occupation', "
                                            . "'$mothers_contact', "
                                            . "'$fathers_name', "
                                            . "'$fathers_qualifications', "
                                            . "'$fathers_occupation', "
                                            . "'$fathers_contact', "
                                            . "'$fathers_emailid', "
                                            . "'$local_guardian', "
                                            . "'$local_guardian_contact', "
                                            . "'$local_guardian_occupation', "
                                            . "'$last_school_attended', "
                                            . "'$last_school_scholar', "
                                            . "'$last_class', "
                                            . "'$passed_year', "
                                            . "'$passed_board', "
                                            . "'$declaration', "
                                            . "now())";
                               if ($conn->query($sql) === TRUE) {
                                echo "New record created successfully";
                                      // sql query execution function
                                                move_uploaded_file($_FILES["file"]["tmp_name"],
                                                "assets/images/registrationpics/" . $_FILES["file"]["name"]);
                                                //echo "<div class='sucess'>"."Stored in: " .
                                                //        
                                                //"upload/" . $_FILES["file"]["name"]."</div>";
                                                ?>
                                                <script type="text/javascript">
                                                alert('Thank you for Registration ');
                                                window.location.href='registration.php';
                                                </script>
                                                <?php
                                } else {
                                 ?>
  <script type="text/javascript">
  alert('error occured while inserting your data');
  </script>
  <?php   
                                echo "Error: " . $sql . "<br>" . $conn->error;
                                }
                                    
                                $conn->close();
                                
                                }
}
}
else
{
echo "<div class='error'>Invalid file</div>";
}//end Image Upload                                                                    
                                            
 }
                              
                                ?>
                                    </div>
                                     
                                    

                                  
                                    
                                  </div>
                                 </div>
                        </section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/SelDownloads.js"></script>

	</body>
</html>
