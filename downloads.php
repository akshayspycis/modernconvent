<?php include 'includes/session.php'; ?>    
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!--Developed by Infopark India, Developer - Lalit Pastor &  Akshay Bilani -->
        <head>
            <meta charset="utf-8">
            <title>Download Section </title>
            <meta name="description" content="">
            <meta name="author" content="">
            <!-- Mobile Meta -->
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <?php include 'includes/csslinks.php';?>
        </head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  transparent-header " onload="loadHTML('downloads')">
            <!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		   <!-- header-container start -->
			<?php include 'includes/header.php'; ?>
			    
                        <div class="banner dark-translucent-bg" style="position:relative;z-index:0;background-image:url('assets/images/bg/22.jpg'); background-position: 50% 27%;">
                            <!-- breadcrumb start -->
                            <!-- ================ -->
                            <div class="breadcrumb-container object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
                                <div class="container">
                                    <ol class="breadcrumb">
                                        <li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
                                        <li class="active">Download Section</li>
                                   </ol>
                                </div>
                            </div>
                            <!-- breadcrumb end -->
                            <div class="container">
                                
                            </div>
                        </div>
                            <div id="page-start"></div>
     <div class="container-fluid" id="new-ticker">
                            <div class="container">
                                <div class="col-md-2">
<!--                                    <p id="para-font">&nbsp;
                                        <b style="color:yellow">Latest News</b><i class="fa fa-caret-right pr-5 pl-10"></i>
                                     </p>-->
                                </div>
                            <div class="col-md-10">

                                   <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();" >
                                       <a href = "assets/pdf/admission-form.pdf" target="_blank" style="color:black;font-weight:bold;">Admissions open for the academic year 2017-18 in Modern Group of Schools.<span style="color:yellow">Click here to Download Form </span>For details contact<span style="color:#f1fa00;font-weight:bold" >
                                               <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": 
                                                                                                                    echo '7566931851, 9713003663';  
                                                                                                                    break;
                                                                                                                 case "2": 
                                                                                                                  echo '7049923643, 9926740041'; 
                                                                                                                       break;
                                                                                                                case "3":
                                                                                                                    echo '9926740041'; 
                                                                                                                      break;
                                               }}
                                                                                                                ?>
                                              
                                           
                                           </span> . Hurry! Only few seats left.</a>
                                  </marquee>
                               </div>
                            </div>
                             
                        </div>
                            <div class="clearfix"></div>
                            <!-- section end -->
                            <!-- section start -->
                            <!-- ================ -->
                         <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container" >
					<div class="row">
						<div class="col-md-12">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;"><strong>Downloads</strong> </h1>
                                                    <div class="separator"></div>
                                            </div>
                                    <p>&nbsp;</p>
                                 </div>
                                <div class="row">
                                    <div class="col-md-12" id="selDownload">
<!--                                      <div class="col-md-11" id="selDownload2">
                                        <div class="media margin-clear" id="student-downloads" >
                                            <div class="col-md-11">
                                                <h5 class="" id="heading-font">SBI PO Preliminary Grand Test SPP-170203 Solutions	</h5>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="testimonial-image">
                                                    <a href="" target="_blank"> <img src="assets/images/bg/downloads.png" id="downloadImage" alt="" title="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                                                    
                                            
                                        </div>
                                    </div>-->
                                    </div>
                                    
                                    
<!--                                    <div class="col-md-11">
                                        <div class="media margin-clear" id="student-downloads" >
                                            <div class="col-md-11">
                                                <h5 class="" id="heading-font">RBI PO  Test SPP-170303 Solutions	</h5>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="testimonial-image">
                                                    <a href="" target="_blank"> <img src="assets/images/bg/downloads.png" id="downloadImage" alt="" title="" class="img-responsive"></a>
                                                </div>
                                            </div>
                                                                    
                                            
                                        </div>
                                    </div>-->
                                  
                                    
                                </div>
                                 </div>
                        </section>
			
			<div class="clearfix"></div>
			<!-- section end -->
		
			<!-- footer top start -->
			<!-- ================ -->
		
			<!-- footer top end -->
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include './includes/footer.php'; ?>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->

		
                <?php include 'includes/jslinks.php';?>
                <?php include 'includes/userSignup.php';?>
                <?php include 'includes/demoRegistration.php';?>
                <script type="text/javascript" src="ajax/SelDownloads.js"></script>
                <script type="text/javascript">
                   onLoadDownloads();
                </script>
	</body>
</html>
