<style>
    #loginMenu:hover{
        color:yellow;
        cursor:pointer;
        
    }
    #multiDrop li a{
        padding-top:15px;
        padding-bottom:15px;
        
    }
</style>
<div class="header-container">
				<div class="header-top colored object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="400" style="box-shadow: 0 2px 1px rgba(0,0,0, 0.50);border-bottom:1px solid white">
					<div class="container">
						<div class="row">
							<div class="col-xs-3 col-sm-6 col-md-8">
								<!-- header-top-first start -->
								<!-- ================ -->
								<div class="header-top-first clearfix">
									<ul class="social-links circle small clearfix hidden-xs">
<!--										<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                                                <li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
										<li class="linkedin"><a target="_blank" href="http://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
										<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
										<li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
										-->
										
									</ul>
									<div class="social-links hidden-lg hidden-md hidden-sm circle small">
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></button>
                                                                                        <ul class="dropdown-menu dropdown-animation" style="position:relative;z-index:999999">
<!--                                                                                            <li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                                                                            <li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
                                                                                            <li class="linkedin"><a target="_blank" href="http://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
                                                                                            <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                                                                                            <li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
                                                                                            -->
                                                                                        </ul>
                                                                                        
										</div>
									</div> 
									<ul class="list-inline hidden-sm hidden-xs top-links" id="newFont1">
                                                           <li><a href="logouttohome.php"><i class="fa fa-home pr-5 pl-10"></i>Back To Home</a></li>
                                                                                    <?php
                                                                                    if(isset($_SESSION['scat'])){
                                                                                    $scat = $_SESSION['scat'];
                                                                                    switch ($scat) {
                                                                                    case "1": ?>
                                                                                    <li><a><i class="fa fa-phone pr-5 pl-10"></i>7566931851, 9713003663</a></li>
                                                                                    <?php   break;
                                                                                    case "2": ?>
                                                                                    <li><a><i class="fa fa-phone pr-5 pl-10"></i>9926740041, 7049923643</a></li>

                                                                                    <?php    break;
                                                                                    case "3": ?>

                                                                                    <li><a><i class="fa fa-phone pr-5 pl-10"></i>9926740041</a></li>
                                                                                    <?php    break;


                                                                                    }

                                                                                    }
                                                                                    ?> 
      
                                                          
                                                                       
         
                                                                            <!--<li><a href="blog.php"><i class="fa fa-caret-right pr-5 pl-10"></i>Blog</a></li>-->
                                                                                <!--<li id="career"><a target="_blank" href="career.php"><i class="fa fa-caret-right pr-5 pl-10"></i>Career</a></li>-->
                                                                                  <!--<li><a target="_blank" href=""><i class="fa fa-caret-right pr-5 pl-10"></i>Notifications</a></li>-->
                                                                                <!--<li><a target="_blank" href="http://onlinebankexams.org/client/pages/home.html" style="color:yellow"><i class="fa fa-caret-right pr-5 pl-10"></i>Online Exam</a></li>-->
										
									</ul>
								</div>
								<!-- header-top-first end -->
							</div>
							<div class="col-xs-9 col-sm-6 col-md-4">

								<!-- header-top-second start -->
								<!-- ================ -->
								<div id="header-top-second"  class="clearfix">
                                                                        <?php   
                                                                        if(isset($_SESSION['user_id'])){
                                                                        if($_SESSION['user_type']==='normal'){
                                                                        ?>
                                                                        <div class="header-top-dropdown text-right">
                                                                        <div class="btn-group ">
                                                                        <p style="position:relative;top:8px;right:5px;font-family:verdana;"><?php echo ""." "."<b>".$_SESSION['user_name']."</b>"; ?><p>
                                                                        </div>  
                                                                        <div class="btn-group">
                                                                            <a href="refer-a-friend.php" class="btn  btn-login btn-sm" id="loginMenu"><b>Refer friend</b></a>
                                                                        </div>  
                                                                        <div class="btn-group">
                                                                            <a href="thoughtday.php" class="btn  btn-login btn-sm" id="loginMenu"><b>Post Thought</b></a>
                                                                        </div>  
                                                                        <div class="btn-group">
                                                                        <a href="logout.php" class="btn  btn-login btn-sm" id="loginMenu"><b>Sign Out</b></a>
                                                                        </div>  
                                                                        </div>

                                                                        <?php

                                                                        }else{
                                                                        header("Location:v-admin/home.php");
                                                                        }
                                                                        }else{
                                                                        ?>
									<!-- header top dropdowns start -->
									<!-- ================ -->
									<div class="header-top-dropdown text-right" id="newFont1">
										<div class="btn-group">
                                                                                    <!--<button  class="btn btn-default btn-sm"  data-toggle="modal" data-target="#mySignUp"><i class="fa fa-user pr-10"></i> Sign Up</button>-->
										</div>
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="modal" data-target="#mySignIn"><i class="fa fa-lock pr-10"></i> Login</button>
											
										</div>
									</div>
                                                                        <?php
            }
            ?>
									<!--  header top dropdowns end -->
								</div>
								<!-- header-top-second end -->
							</div>
						</div>
					</div>
				</div>
				<!-- header-top end -->
				
				<!-- header start -->
				<!-- classes:  -->
				<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
				<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
				<!-- "full-width": mandatory class for the full-width menu layout -->
				<!-- "centered": mandatory class for the centered logo layout -->
				<!-- ================ --> 
                                <div class="container">
                                    <div class="row" style="position:relative;top:10px;margin-bottom:10px">
                                                    <div class="col-md-1 col-xs-2" id="headbanner">
                                                         <div id="logoImg" class="logo text-center object-non-visible animated object-visible fadeInLeft" data-animation-effect="fadeInLeft" data-effect-delay="600" >
                                                             <p><a href="index.php"><img id="logo_img" src="assets/images/logomoderngroup2.png" alt="Modern Public School" style="" ></a></p>
							</div>
                                                    </div>
                                                    <div class="col-md-11 col-xs-10">
                                                                    <?php
                                                                    if(isset($_SESSION['scat'])){
                                                                    $scat = $_SESSION['scat'];
                                                                    switch ($scat) {
                                                                    case "1": ?>
                                                                    <h1 id="headheading">Modern Convent School Mandideep</h1>
                                                                    <p class="text-center" id="headpara">&nbsp;</p>
                                                                    <?php   break;
                                                                    case "2": ?>
                                                                    <h1 id="headheading" >Modern Convent School Obedullaganj</h1>
                                                                    <p class="text-center" id="headpara">&nbsp;</p>

                                                                    <?php    break;
                                                                    case "3": ?>
                                                                    <h1 id="headheading"  style="text-align:center">Modern English Medium Higher Secondary School Tamot</h1>
                                                                    <p class="text-center" id="headpara">(Recognized by MP Govt. &amp; Madhya Pradesh Board of Secondary Education)</p>

                                                                    <?php    break;


                                                                    }

                                                                    }
                                                                    ?>
                                                   </div>
                                                    </div>
                                </div>
                                <header class="header  fixed   clearfix" style="box-shadow: 0 4px 10px rgba(0,0,0, 0.50);">
                                    <div id="wrap-nav">
                                        <div class="container">
						
                                                    <div class="row">
							<div class="object-non-visible animated object-visible fadeInRight" data-animation-effect="fadeInRight" data-effect-delay="600">
                                                                <!-- header-right start -->
								<!-- ================ -->
                                                                <div class="header-right clearfix" >
								<!-- main-navigation start -->
								<!-- classes: -->
								<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
								<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
								<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
								<!-- ================ -->
                                                                <div class="main-navigation  animated with-dropdown-buttons " >
<!-- navbar start -->                                               <!-- ================ -->
									<nav class="navbar navbar-default" role="navigation" id="newFont1" >
										<div class="container-fluid">

											<!-- Toggle get grouped for better mobile display -->
											<div class="navbar-header">
												<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
												
											</div>

											<!-- Collect the nav links, forms, and other content for toggling -->
											<div class="collapse navbar-collapse" id="navbar-collapse-1" >
												<!-- main-menu -->
                                                                                               
                                                                                                <ul class="nav navbar-nav" id="multiDrop">
                                                                                                    <li id="home"><a  href="index.php">Home</a></li>
                                                                                                  
                                                                                                        <li id="about-us"><a  href="about-us.php">About Us</a></li>
                                                                                                    <!-- mega-menu start -->
													<!-- mega-menu end -->
													<li id="registration"class="dropdown">
														<a class="dropdown-toggle" data-toggle="dropdown" href="#">Registration </a>
														<ul class="dropdown-menu">
                                                                                                                    <li><a href="assets/pdf/admission-form.pdf" target="_blank">Download Admission Form</a></li>
                                                                                                                    <li><a href="registration.php">Apply Online</a></li>
                                                                                                                    
                                                                                                                </ul>
                                                                                                        </li>
                                                                                                      
                                                                                                             <li id="downloads"><a href="downloads.php">Downloads</a></li>
                                                                                                                    <!--<li><a href="#">Recommend Books </a></li>
                                                                                                                    <!--<li><a href="#">Personality Test</a></li>-->
                                                                                                                 <?php   if(isset($_SESSION['scat'])){
                                                                                                                $scat = $_SESSION['scat'];
                                                                                                                switch ($scat) {
                                                                                                                case "1": ?>
                                                                                                                     <li id="toppers" style="display:none"><a href="ourpride.php">Our Topper's</a></li>
                                                                                                               <?php   break;
                                                                                                                          case "2": ?>
                                                                                                                     <li id="toppers" style="display:none"><a href="ourpride.php">Our Topper's</a></li>
                                                                                                                     <?php   break;
                                                                                                                           case "3": ?>
                                                                                                                       <li id="toppers"><a href="ourpride.php">Our Topper's</a></li>
                                                                                                                 <?php } }?>
                                                                                                                    <li id="facilities"><a href="facilities.php">Facilities</a></li>
                                                                                                                    <!--<li id="testimonials"><a href="testimonials.php">Parent's Speak</a></li>-->
                                                                                                                   
<!--                                                                                                        <li id="students" class="dropdown">
														<a class="dropdown-toggle" data-toggle="dropdown" href="#">Students </a>
														<ul class="dropdown-menu"  >
                                                                                                                     <li><a href="#">Latest Vacancies with your eligibility</a></li>
                                                                                                                    <li><a href="downloads.php">Downloads</a></li>
                                                                                                                    <li><a href="#">Recommend Books </a></li>
                                                                                                                    <li><a href="#">Personality Test</a></li>
                                                                                                                    <li><a href="testimonials.php">Students Speak</a></li>
                                                                                                                    <li><a href="ourselections.php">Our Topper's</a></li>
                                                                                                                </ul>
													</li>-->
                                                                                                        
                                                                                                     <li id="gallery"><a  href="gallery.php">Gallery</a></li>	
                                                                                                        <li id="contact"><a  href="contactus.php">Contact Us</a></li>	
												</ul>
                                                                                               
                                                                                         
                                                                                               
												<!-- main-menu end -->
												
												
												
											</div>

										</div>
									</nav>
									<!-- navbar end -->

								</div>
								<!-- main-navigation end -->	
								</div>
								<!-- header-right end -->
					
							</div>
						</div>
					</div>
                                    </div>
					
					
				</header>
				<!-- header end -->
			</div>



<style>
    .sidenav {
        height:100%;
        width: 20%;
        position: fixed;
        z-index:99999;
        top:0;
        right: 0;
        background-color:rgba(0,0,0,0.7);
        overflow-x: hidden;
        transition: 0.5s;
        padding-top: 60px;
        
    }
    
    .sidenav a {
        padding: 8px 8px 8px 32px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;
        display: block;
        transition: 0.3s
    }
    
    .sidenav a:hover, .offcanvas a:focus{
        color: #f1f1f1;
    }
    
    .sidenav .closebtn {
        position: absolute;
        top: 0;
        right: 25px;
        font-size: 36px;
        margin-left: 50px;
    }
    
    @media screen and (max-height: 450px) {
        .sidenav {padding-top: 15px;}
        .sidenav a {font-size: 18px;}
    }
    #quick-query{
        font-family:century gothic;
        font-weight:bold;
      
    }
</style>

<button id="quick-querybtn" class="btn" style="background:#0d5995; position:fixed;top:400px;right:0px;z-index:9999;box-shadow:-5px 3px 2px 0 rgba(0,0,0,0.2);font-family:century gothic;color:white;font-size:16px" onclick="openNav()"><b>Q<br>u<br>e<br>r<br>y</b></button>
<div id="mySidenav" class="sidenav" style="display:none">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <h4 class="heading-font" style="margin-left:30px;margin-bottom:0px;color:white;font-weight:bold">Drop a Query</h4>
    <form role="form" class="p-30" method="post" id="quick-query">
        <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
            <input type="hidden" class="form-control" id="school_id" name="school_id" value="<?php echo $scat ?>" placeholder="Name">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
        </div>
        <div class="form-group">
            
            <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact">
        </div>
        <div class="form-group">
            
            <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
        </div>
        
        <div class="form-group">
            
            <textarea class="form-control" rows="3" id="message" name="message" placeholder="Message"></textarea>
        </div>
        <input type="submit" name="submit" id="submit" class="btn btn-default" value="Send"/>
    </form>
   

<!-- Modal -->
<div id="warningmsg" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:orangered;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Warning</h4>
      </div>
      <div class="modal-body" id="errormsg">
          <p id="msg" style="color:green;font-family:century gothic;font-size:18px;font-weight:bold"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

</div> 



<div id="enquirysuccess" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header" style="background:green;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Success</h4>
      </div>
      <div class="modal-body" id="errormsg">
          <p id="msg" style="color:gray;font-family:verdana;font-size:14px;line-height:22px">Thank you for getting in touch! We appreciate you contacting us. We try to respond as soon as possible. Have a great day ahead!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.display = "block";
        document.getElementById("mySidenav").style.width = "300px";
    }
    
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
    

</script>  
<script type="text/javascript" src="ajax/QuickQuery.js"></script>
<script type="text/javascript">
    function loadHTML(id){
        $('#'+id).addClass('active');
    }    

</script>
  <script>
        $(window).resize(function(){
//            var ws = $(document ).width();
             if($(document).width()<668){
             
             $("#quick-querybtn").css({'display':'none'});
            $("#logoImg").css({'position':'relative'});
           $("#headheading").css({'font-size':'16px','line-height':'36px','margin-bottom':'10px'});
            $("#logoImg").find("#logo_img").width('50px').height('50px')
           $("#headpara").css({'display':'none'});
       }else{
           $("#quick-querybtn").css({'display':''});
           $("#logoImg").css({'position':'relative','text-align':'center','left':'','width':'','margin-left':'','margin-right':''});
           $("#headheading").css({'position':'relative','top':'','font-size':'','line-height':'','margin-bottom':''});
           $("#logoImg").find("#logo_img").width('100px').height('100px')
           $("#headpara").css({'display':'block'}); 
       } 
            
        }
        
    );
          if($(document).width()<668){
             $("#quick-querybtn").css({'display':'none'});
            $("#logoImg").css({'position':'relative'});
           $("#headheading").css({'font-size':'16px','line-height':'36px','margin-bottom':'10px'});
            $("#logoImg").find("#logo_img").width('50px').height('50px')
           $("#headpara").css({'display':'none'});
       }else{
           $("#quick-querybtn").css({'display':''});
           $("#logoImg").css({'position':'relative','text-align':'center','left':'','width':'','margin-left':'','margin-right':''});
           $("#headheading").css({'position':'relative','top':'','font-size':'','line-height':'','margin-bottom':''});
           $("#logoImg").find("#logo_img").width('100px').height('100px')
             $("#headpara").css({'display':'block'}); 
       } 

    </script>