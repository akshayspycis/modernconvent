<footer id="footer" class="clearfix ">
                            <div class="container-fluid" id="footer-top"></div>
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" id="footer-middle">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <h2 class="title">Our Motto</h2>
                                                                            <div class="separator-2"></div>
										
                                                                            <p id="cpara"><b>"Strengthening the student talent to become a successful entrepreneur, which will have way for employment opportunities and sustainability in all spheres of life..... </b><a href="about-us.php">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a></p>
										<div class="separator-2"></div>
										
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <h2 class="title">Admissions</h2>
                                                                            <div class="separator-2"></div>
                                                                            <ul class="list-icons" style="font-weight:bold">
                                                                                 <?php
                                                        if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];

                                                        switch ($scat) {
                                                        case "1": ?>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Pre School</li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 1st to Class 5th</li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 6th to Class 8th</li>
                                                     <?php   break;
                                                        case "2": ?>
                                                         <li><i class="fa fa-book pr-10 text-default"></i>Pre School</li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 1st to Class 5th</li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 6th to Class 8th</li>
                                                    
                                                    <?php    break;
                                                        case "3": ?>
                                                        <li><i class="fa fa-book pr-10 text-default"></i>Pre School</li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 1st to Class 5th</li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 6th to Class 8th</li>
                                                                  <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 9th to Class 10th</li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i>Class 10th to Class 12th</li>
                                                                
                                                    
                                                    <?php    break;
                                                       
                                                        
                                                        }
                                                        }   ?>    
                                                                              
                                                                                                            
                                                                            </ul>
                                                                            
										<div class="separator-2"></div>
										
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <h2 class="title">Beyond Academics</h2>
                                                                            <div class="separator-2"></div>
                                                                            <ul class="list-icons" style="font-weight:bold">
                                                                                    
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Sports and Games</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Outdoor Activities</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Music, Dance & Drama</li>
                                                                                <div class="separator-2"></div>
                                                                                <li><i class="fa fa-angle-double-right pr-10 text-default"></i>Literary Activities</li>
                                                                               <div class="separator-2"></div>
                                                                               
                                                                                                            
                                                                            </ul>
                                                                            
										
										
									</div>
								</div>
								<div class="col-md-3">
                                                       <?php
                                                        
                                                        if(isset($_SESSION['scat'])){
                                                            $scat = $_SESSION['scat'];
                                                        switch ($scat) {
                                                        case "1": ?>
                                                               <div class="footer-content">
                                                                            <h2 class="title">Find Us</h2>
                                                                            <div class="separator-2"></div>
<!--                                                                            <p>Find us on Social Media</p>
                                                                            <ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
											<li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
										</ul>
                                                                            <div class="separator-2"></div>-->
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
												<i class="fa fa-location-arrow fa-3x pr-10 text-default"></i>
											</div>
											<div class="media-body">
                                                                                            <h6 class="media-heading" style="line-height:24px;">B – 205 , Indra Nagar , Mandideep, Distric Raisen ,<br> M.P - 462046</h6>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
                                                                                            <i class="fa fa-phone fa-3x pr-10 text-default"></i>
												
											</div>
											<div class="media-body">
                                                                                            <h6 class="media-heading" style="line-height:24px;"> 9926740041,  7566931851, 9713003663</h6>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
                                                                                            <i class="fa fa-map-marker fa-3x pr-10 text-default"></i>
												
											</div>
											<div class="media-body">
                                                                                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4364.398917758895!2d77.51381494153914!3d23.09682477961176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x397c47cab1454ba1%3A0x926d7d8245c1c17b!2sModern+Convent+School!5e0!3m2!1sen!2s!4v1492077651889" width="250" height="100" frameborder="0" style="border:0" allowfullscreen></iframe>
											</div>
											
										</div>
                                                                            
										
										
									</div>
                                                     <?php   break;
                                                        case "2": ?>
                                                           <div class="footer-content">
                                                                            <h2 class="title">Find Us</h2>
                                                                            <div class="separator-2"></div>
<!--                                                                            <p>Find us on Social Media</p>
                                                                            <ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
											<li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
										</ul>
                                                                            <div class="separator-2"></div>-->
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
												<i class="fa fa-location-arrow fa-3x pr-10 text-default"></i>
											</div>
											<div class="media-body">
                                                                                            <h6 class="media-heading" style="line-height:24px;">Mahaveer Colony C.L Nagar, Obedullaganj, Raisen , M.P , Pin code- 464993</h6>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
                                                                                            <i class="fa fa-phone fa-3x pr-10 text-default"></i>
												
											</div>
											<div class="media-body">
                                                                                            <h6 class="media-heading" style="line-height:24px;"> 9926740041,  7049923643</h6>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
                                                                                            <i class="fa fa-map-marker fa-3x pr-10 text-default"></i>
												
											</div>
											<div class="media-body">
                                                                                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.5483562871996!2d77.58331391496745!3d23.003629884962695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDAwJzEzLjEiTiA3N8KwMzUnMDcuOCJF!5e0!3m2!1sen!2s!4v1491356846185" width="250" height="100" frameborder="0" style="border:0" allowfullscreen></iframe>
											</div>
											
										</div>
                                                                            
										
										
									</div>
                                                    
                                                    <?php    break;
                                                        case "3": ?>
                                                         <div class="footer-content">
                                                                            <h2 class="title">Find Us</h2>
                                                                            <div class="separator-2"></div>
<!--                                                                            <p>Find us on Social Media</p>
                                                                            <ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
											<li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
											<li class="youtube"><a target="_blank" href="http://www.youtube.com/"><i class="fa fa-youtube-play"></i></a></li>
										</ul>
                                                                            <div class="separator-2"></div>-->
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
												<i class="fa fa-location-arrow fa-3x pr-10 text-default"></i>
											</div>
											<div class="media-body">
                                                                                            <h6 class="media-heading" style="line-height:24px;">NH-12, Main Road, Raisen M.P,<br> Pin code- 464991</h6>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
                                                                                            <i class="fa fa-phone fa-3x pr-10 text-default"></i>
												
											</div>
											<div class="media-body">
                                                                                            <h6 class="media-heading" style="line-height:24px;"> 9926740041</h6>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
                                                                                            <i class="fa fa-map-marker fa-3x pr-10 text-default"></i>
												
											</div>
											<div class="media-body">
                                                                                           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.477819659714!2d77.63907721496751!3d23.006221784961305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDAwJzIyLjQiTiA3N8KwMzgnMjguNiJF!5e0!3m2!1sen!2s!4v1491357037410" width="250" height="100" frameborder="0" style="border:0" allowfullscreen></iframe>
											</div>
											
										</div>
                                                                            
										
										
									</div>
                                                    
                                                    <?php    break;
                                                       
                                                        
                                                        }
                                                        } ?>   
									
								</div>

							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="footer-top">
					<div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
                                                                    <p  id="cpara2">Copyright © 2017 <b>Modern Group of Schools</b>. All Rights Reserved. Powered by <a href="http://infoparkinnovations.in" target="_blank" style="color:white"><b>Infopark</b></a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
